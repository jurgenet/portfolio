# Примерочная

## Table Of Content 
---
  * [Mel](#markdown-header-mel) :
    * [Screens](#markdown-header-mel-screens)
        *  [Avatar Uploader](#markdown-header-mel-avatar-uploader)
        *  [Feed (Xl)](#markdown-header-mel-feed-xl)
        *  [Feed (L)](#markdown-header-mel-feed-l)
        *  [Feed (M)](#markdown-header-mel-feed-m)
        *  [Feed Not Authorized](#markdown-header-mel-feed-not-authorized)
        *  [Feed News (XL)](#markdown-header-mel-feed-news-xl)
        *  [Feed News (M)](#markdown-header-mel-feed-news-m)
        *  [Fresh](#markdown-header-mel-fresh)
        *  [Popular Authors](#markdown-header-mel-popular-authors)
    * [Code](#markdown-header-mel-code) :  
        *  [b-popular-authors.js](#markdown-header-mel-b-popular-authors-js)
        *  [b-popular-authors.scss](#markdown-header-mel-b-popular-authors-scss)
        *  [b-popular-authors.soy](#markdown-header-mel-b-popular-authors-soy)
        *  [view.js](#markdown-header-mel-b-popular-authors-view-js)
  * [Sber Health](#markdown-header-sber-health) :  
    * [Screens](#markdown-header-sber-health-screens) :   
        * [Doctor search (XL)](#markdown-header-doctor-search-xl)
        * [Doctor search + Dropdown 1 (XL)](#markdown-header-doctor-search-dropdown1-xl)
        * [Doctor search + Dropdown 2 (XL)](#markdown-header-doctor-search-dropdown2-xl)
        * [Doctor search Sticky Menu (XL)](#markdown-header-doctor-search-sticky-menu-xl)
        * [Doctor search (L)](#markdown-header-doctor-search-l)
        * [Doctor search (M)](#markdown-header-doctor-search-m)
    *  [Code](#markdown-header-sber-health-code) 
        * l-doctor-search (layout)
            * [l-doctor-search.js](#markdown-header-l-doctor-search-js)
            * [l-doctor-search.scss](#markdown-header-l-doctor-search-scss)
            * [l-doctor-search.soy](#markdown-header-l-doctor-search-soy)
            * [view.js](#markdown-header-l-doctor-search-view-js) 
               * b-doctor-search 
                  * [b-doctor-search.js](#markdown-header-b-doctor-search-js)
                  * [b-doctor-search.scss](#markdown-header-b-doctor-search-scss)
                  * [b-doctor-search.soy](#markdown-header-b-doctor-search-soy)
                  * [view.js](#markdown-header-b-doctor-search-view-js)
               * b-doctor-list
                  * [b-doctor-list.js](#markdown-header-b-doctor-list-js)
                  * [b-doctor-list.scss](#markdown-header-b-doctor-list-scss)
                  * [b-doctor-list.soy](#markdown-header-b-doctor-list-soy)
                  * [view.js](#markdown-header-b-doctor-list-view-js)
              * b-doctor-item
                  * [b-doctor-item.js](#markdown-header-b-doctor-item-js)
                  * [b-doctor-item.scss](#markdown-header-b-doctor-item-scss)
                  * [b-doctor-item.soy](#markdown-header-b-doctor-item-soy)
                  * [view.js](#markdown-header-b-doctor-item-view-js)
  * [Castle Quiz](#markdown-header-castle-quiz)  
    * [Code](#markdown-header-castle-quiz-code) 
        * i-sound  
             * [i-sound.js](#markdown-header-i-sound-js)  
             * [i-web-media.js](#markdown-header-i-web-media-js)  
             * [i-cordova-media.js](#markdown-header-i-cordova-media-js)  
             * [i-cordova-media-sprite.js](#markdown-header-i-cordova-media-sprite-js)  

## Mel ##
---
[mel.fm](https://mel.fm/)

> stack: Node.js / Google Closure Tools (Compiler/Library/Soy template) / ES5  
  
> Мои задачи:  
> - функциональность загрузки аватара в профиле  
> - блок "свежее на меле"  
> - блок "популярные авторы"  
> - блок "рекомендуем"  
> - раздел "подписки" включает себя ленту новостей, с бесконечной подгрузкой и с фильтром сортировки на "новость"/"статья"  
> - блок рекомендаций, рендерится когда незарегистрированный пользователь зашел на раздел "мои подписки", так же этот блок рендерится на ленту новостей зарегистрированного пользователя если новости для подгрузки закончились   
> - В админской части сайта - раздел для модерирования блогов редакцией  
> - Верстка письма с сообщением о блокировке поста   

### Mel Screens ###
---
[(на верх)](#markdown-header-table-of-content)


### Mel Avatar Uploader ###
[(на верх)](#markdown-header-table-of-content)

![XL](pb/img/avatar-uploader.png)

### Mel Feed Xl ###
[(на верх)](#markdown-header-table-of-content)

![XL](pb/img/feed-xl.png)

### Mel Feed L ###
[(на верх)](#markdown-header-table-of-content)

![XL](pb/img/feed-l.png)

### Mel Feed M ###
[(на верх)](#markdown-header-table-of-content)

![XL](pb/img/feed-m.png)

### Mel Feed News Xl ###
[(на верх)](#markdown-header-table-of-content)

![XL](pb/img/feed-news-xl.png)

### Mel Feed News M ###
[(на верх)](#markdown-header-table-of-content)

![XL](pb/img/feed-news-m.png)

### Mel Feed Not Authorized ###
[(на верх)](#markdown-header-table-of-content)

![XL](pb/img/feed-not-authorized.png)

### Mel Fresh ###
[(на верх)](#markdown-header-table-of-content)

![XL](pb/img/fresh.png)

### Mel Popular Authors ###
[(на верх)](#markdown-header-table-of-content)

![XL](pb/img/popular-authors.png)

### Mel Code ###
---
[(на верх)](#markdown-header-table-of-content)

### Mel b-popular-authors js ###
[(на верх)](#markdown-header-table-of-content)

```js
goog.provide('pb.bPopularAuthors.PopularAuthors');

goog.require('cl.gButton.Button');
goog.require('cl.iControl.Control');
goog.require('cl.iRequest.Request');
goog.require('goog.events');
goog.require('pb.bPbAuthor.PbAuthor');

goog.scope(function() {



/**
 * Popular authors control
 * @param {pb.bPopularAuthors.View} view
 * @param {Object=} opt_domHelper
 * @constructor
 * @extends {cl.iControl.Control}
 */
pb.bPopularAuthors.PopularAuthors = function(view, opt_domHelper) {
    PopularAuthors.base(this, 'constructor', view, opt_domHelper);

    this.setSupportedState(goog.ui.Component.State.ALL, false);

    /**
     * @type {Array}
     * @private
     */
    this.authorBuffer_ = null;

    /**
     * @type {number}
     * @private
     */
    this.authorDisplayCount_ = null;

    /**
     * @type {Array<pb.bPbAuthor.PbAuthor>}
     * @private
     */
    this.authorList_ = null;

    /**
     * @private
     * @type {cl.gButton.Button}
     */
    this.refreshButton_ = null;
};
goog.inherits(pb.bPopularAuthors.PopularAuthors, cl.iControl.Control);

var PopularAuthors = pb.bPopularAuthors.PopularAuthors;
var Author = pb.bPbAuthor.PbAuthor;
var Button = cl.gButton.Button;
var Request = cl.iRequest.Request;


/**
 * @override
 * @param {Element} element
 */
PopularAuthors.prototype.decorateInternal = function(element) {
    PopularAuthors.base(this, 'decorateInternal', element);

    this.authorBuffer_ = this.getViewParams().authorBuffer;
    this.authorDisplayCount_ = this.getViewParams().authorDisplayCount;

    var dom = this.getView().getDom();

    this.authorList_ = this.decorateChildren('pbAuthor', dom.authorList);

    if (dom.refreshButton) {
        this.refreshButton_ = this.decorateChild('button', dom.refreshButton);
    }
};


/**
 * @override
 */
PopularAuthors.prototype.enterDocument = function() {
    PopularAuthors.base(this, 'enterDocument');

    this.getHandler().listen(
        this,
        Author.Event.SUBSCRIBER_CLICK,
        this.onAuthorClick_
    );

    if (this.refreshButton_) {
        this.listenToRefreshEvent_();
    }
};


/**
 * @private
 * @param {Object} event
 */
PopularAuthors.prototype.onAuthorClick_ = function(event) {
    this.sendAnalytics_(event.data.isSubscribed);

    var author = event.target;
    var authorId = event.data.id;
    var isSubscribed = event.data.isSubscribed;

    Request.getInstance().send({
        url: '/feed/author/' + authorId,
        type: isSubscribed ? 'delete' : 'post'
    }).then(
        function() {
            author.toggleSubscriberState();
        }
    );
};


/**
 * @private
 */
PopularAuthors.prototype.onRefreshButtonClick_ = function() {
    this.refreshButton_.playAnimation();

    this.authorList_.forEach(function(author) {
        author.dispose();
    });

    // Roll author list
    for (var i = 0; i < this.authorDisplayCount_; i++) {
        this.authorBuffer_.push(this.authorBuffer_.shift());
    }

    var nextAuthors = this.authorBuffer_.slice(0, this.authorDisplayCount_);

    this.getView().changeAuthors(nextAuthors)
        .thenAlways(
            function() {
                this.authorList_ = this.decorateChildren(
                    'pbAuthor',
                    this.getView().getDom().authorList
                );

                this.refreshButton_.stopAnimation();

                this.listenToRefreshEvent_();
            },
            this
        );
};


/**
 * @private
 */
PopularAuthors.prototype.listenToRefreshEvent_ = function() {
    this.getHandler().listenOnce(
        this.refreshButton_,
        Button.Event.CLICK,
        this.onRefreshButtonClick_
    );
};


/**
 * @private
 * @param {boolean} isSubscribed
 */
PopularAuthors.prototype.sendAnalytics_ = function(isSubscribed) {
    if (window.ga) {
        var eventAction,
            eventCategory;

        if (isSubscribed) {
            eventAction = 'bAuthorUnsubscribeRightFeed';
            eventCategory = 'Feed';
        } else {
            eventAction = 'bAuthorSubscribeRightFeed';
            eventCategory = 'CreateFeed';
        }

        window.ga(
            'send',
            'event',
            eventCategory,
            eventAction,
            'authorName'
        );
    }
};

});  // goog.scope

```

### Mel b-popular-authors scss ###
[(на верх)](#markdown-header-table-of-content)

```scss
.b-popular-authors {
    box-sizing: border-box;
    width: 300px;
    margin-top: 25px;
    padding-bottom: 10px;
    border: 1px solid #E7E7E7;
    background-color: #F7F7F7;
}

.b-popular-authors__caption {
    display: inline-block;

    margin: 15px 0 0 20px;

    color: #000000;

    font: bold 18px 'Proxima', sans-serif;
    line-height: 1.22;
}

.b-popular-authors__refresh-button {
    display: inline-block;

    margin-left: 10px;

    vertical-align: middle;
}

.b-popular-authors__author-list {
    padding: 0 0 0 20px;
}

.b-popular-authors__author-frame {
    padding: 0 0 5px 0;
}

.b-popular-authors__author-list {
    &.b-popular-authors__author-list_animation {
        -webkit-animation: fade-out 0.2s ease-in;
        animation: fade-out 0.2s ease-in;
    }
}

```

### Mel b-popular-authors soy ###
[(на верх)](#markdown-header-table-of-content)

```soy
{namespace pb.bPopularAuthors.Template}


/**
 * Popular authors control params
 * @typedef {
 *     data: {
 *         authorList: {
 *             authors: Array<pb.bPbAuthor.Temaplate.Params>,
 *             displayCount: number
 *         }
 *     }
 * } pb.bPopularAuthors.Template.Params
 */


/**
 * Popular authors control
 * @param params {pb.bPopularAuthors.Template.Params}
 * @extends cl.iControl.Template
 * @constructor
 */
{template .authors}
     {call .init data="all"/}
{/template}


/**
 * Body template
 * @param params {pb.bPopularAuthors.Template.Params}
 */
{template .body}
    <div class="{call .rootClass /}__caption">
        Лучшие авторы
    </div>
    <div class="{call .rootClass /}__refresh-button">
        {call cl.iFactory.FactoryManager.INSTANCE.renderTemplate}
            {param params: [
                'factory': 'pablo',
                'type': 'button',
                'renderParams': [
                    'data': [
                        'label': 'Ещё авторы',
                        'iconName': 'refresh-btn'
                    ],
                    'config': [
                        'theme': 'inline'
                    ]
                ]
            ] /}
       {/call}
    </div>
    {call .authorList}
        {param authors: $params?.data.authorList.authors /}
        {param displayCount: $params?.data.authorList.displayCount /}
    {/call}
{/template}

/**
 * @param authors {Array<pb.bPbAuthor.Temaplate.Params>}
 * @param displayCount {number}
 */
{template .authorList}
    <div class="{call .rootClass /}__author-list">
        {for $i in range($displayCount)}
            <div class="{call .rootClass /}__author-frame">
                {call cl.iFactory.FactoryManager.INSTANCE.renderTemplate}
                    {param params: [
                        'factory': 'pablo',
                        'type': 'pbAuthor',
                        'renderParams': [
                            'config': [
                                'subscribeControlSize': 'S',
                                'autoToggleSubscribeControl': false
                            ],
                            'data': $authors[$i].data,
                            'additionalClasses': [
                                'type': 'compressed'
                            ]
                        ]
                    ] /}
                {/call}
            </div>
        {/for}
    </div>
{/template}

/**
 * @override
 * @param params {pb.bPopularAuthors.Template.Params}
 */
{template .attributes kind="attributes"}
    {let $dataParams: [
        'authorBuffer': $params.data.authorList.authors,
        'authorDisplayCount': $params.data.authorList.displayCount
    ] /}
    data-params="{call goog.json.serialize data="$dataParams" /}"
{/template}

/**
 * Root CSS class template
 */
{template .rootClass}
    b-popular-authors
{/template}

```

### Mel b-popular-authors view js ###
[(на верх)](#markdown-header-table-of-content)

```js
goog.provide('pb.bPopularAuthors.View');

goog.require('cl.gButton.View');
goog.require('cl.iControl.View');
goog.require('cl.iUtils.Utils');
goog.require('goog.dom');
goog.require('goog.dom.classlist');
goog.require('goog.events.EventType');
goog.require('goog.soy');
goog.require('goog.style.transition');
goog.require('pb.bPbAuthor.View');
goog.require('pb.bPopularAuthors.Template');

goog.scope(function() {



/**
 * Popular authors View
 * @param {Object=} opt_params
 * @param {Object=} opt_type
 * @param {string=} opt_modifier
 * @constructor
 * @extends {cl.iControl.View}
 */
pb.bPopularAuthors.View = function(opt_params, opt_type, opt_modifier) {
    pb.bPopularAuthors.View.base(
        this,
        'constructor',
        opt_params,
        opt_type,
        opt_modifier
    );

    /**
     * @type {Element}
     */
    this.dom.authorListContainer = null;

    /**
     * @protected
     * @type {Array<Element>}
     */
    this.dom.authorList = [];

    /**
     * @type {Element}
     */
    this.dom.refreshButton = null;
};
goog.inherits(pb.bPopularAuthors.View, cl.iControl.View);


var View = pb.bPopularAuthors.View;
var ButtonView = cl.gButton.View;
var AuthorView = pb.bPbAuthor.View;


/** @enum {string} */
View.CssClass = {
    ROOT: 'b-popular-authors',
    AUTHOR_LIST_CONTAINER: 'b-popular-authors__author-list',
    REFRESH_BUTTON_CONTAINER: 'b-popular-authors__refresh-button',
    AUTHOR_LIST_ANIMATION: 'b-popular-authors__author-list_animation'
};


/** @override */
View.prototype.decorateInternal = function(element) {
    View.base(this, 'decorateInternal', element);

    this.initAuthorList_();

    this.dom.refreshButton = this.getElementByClass(
        ButtonView.CssClass.ROOT,
        this.getElementByClass(
            View.CssClass.REFRESH_BUTTON_CONTAINER
        )
    );
};


/**
 * @private
 */
View.prototype.initAuthorList_ = function() {
    this.dom.authorListContainer = this.getElementByClass(
        View.CssClass.AUTHOR_LIST_CONTAINER
    );

    this.dom.authorList = this.getElementsByClass(AuthorView.CssClass.ROOT);
};


/**
 * @param {Array} authors
 * @return {goog.Promise}
 */
View.prototype.changeAuthors = function(authors) {
    if (!goog.style.transition.isSupported()) {
        this.reRenderAuthorList_(authors);
        return goog.Promise.resolve();
    }

    var resolver = goog.Promise.withResolver();

    this.getHandler().listen(
        this.dom.authorListContainer,
        goog.events.EventType.ANIMATIONEND,
        function() {
            this.reRenderAuthorList_(authors);
            resolver.resolve();
        }
    );

    goog.dom.classlist.add(
        this.dom.authorListContainer,
        View.CssClass.AUTHOR_LIST_ANIMATION
    );

    return resolver.promise;
};


/**
 * @param {Array} authors
 * @private
 */
View.prototype.reRenderAuthorList_ = function(authors) {
    var updatedAuthorListContainer = goog.soy.renderAsElement(
        pb.bPopularAuthors.Template.authorList, {
            authors: authors,
            displayCount: authors.length
        }, {
            factory: this.getStylization()
        }
    );

    goog.dom.replaceNode(
        updatedAuthorListContainer,
        this.dom.authorListContainer
    );

    this.initAuthorList_();
};

});  // goog.scope

```

## Sber Health ##
---
[(на верх)](#markdown-header-table-of-content)

> мой вклад в проект
![XL](sh/stats.png)

> stack: nodejs (Express/RestAPI) + Google Closure Tools (Compiler/Library/Soy template)
> API от [docdoc](https://docdoc.ru/)  
> Пример одной из страниц. На этой странице НЕ Я делал яндекс карты и график работ.
  

### Sber Health Screens ###
[(на верх)](#markdown-header-table-of-content)

#### Doctor Search XL ####
[(на верх)](#markdown-header-table-of-content)

![XL](sh/doc-search/img/xl.png)

#### Doctor Search Dropdown1 XL ####
[(на верх)](#markdown-header-table-of-content)

![XL](sh/doc-search/img/dropdown_1.png)

#### Doctor Search Dropdown2 XL ####
[(на верх)](#markdown-header-table-of-content)

![XL](sh/doc-search/img/dropdown_2.png)

#### Doctor Search Sticky Menu XL ####
[(на верх)](#markdown-header-table-of-content)

![XL](sh/doc-search/img/sticky_menu.png)

#### Doctor Search L ####
[(на верх)](#markdown-header-table-of-content)

![XL](sh/doc-search/img/l.png)

#### Doctor Search M ####
[(на верх)](#markdown-header-table-of-content)

![XL](sh/doc-search/img/m.png)

### Sber Health Code ###
[(на верх)](#markdown-header-table-of-content)

#### l-doctor-search js ####
[(на верх)](#markdown-header-table-of-content)

```js
goog.provide('sh.lDoctorSearch.DoctorSearch');

goog.require('cl.iRequest.Request');
goog.require('goog.Uri');
goog.require('goog.dom');
goog.require('goog.ui.Component.State');
goog.require('sh.iLayout.LayoutSberHealth');
goog.require('sh.lDoctorSearch.View');
goog.require('sh.lDoctorSearch.bDoctorSearch.DoctorSearch');


goog.scope(function() {



/**
 * DoctorSearch layout
 * @param {sh.lDoctorSearch.View} view
 * @param {goog.dom.DomHelper=} opt_domHelper
 * @constructor
 * @extends {sh.iLayout.LayoutSberHealth}
 */
sh.lDoctorSearch.DoctorSearch = function(view, opt_domHelper) {
    sh.lDoctorSearch.DoctorSearch.base(this, 'constructor',
                                       view, opt_domHelper);

    this.setSupportedState(goog.ui.Component.State.ALL, false);

    /**
     * @type {sh.lDoctorSearch.bDoctorSearch.DoctorSearch}
     * @private
     */
    this.doctorSearchControl_ = null;
};
goog.inherits(sh.lDoctorSearch.DoctorSearch, sh.iLayout.LayoutSberHealth);


var DoctorSearch = sh.lDoctorSearch.DoctorSearch,
    DoctorSearchControl = sh.lDoctorSearch.bDoctorSearch.DoctorSearch,
    Request = cl.iRequest.Request,
    request = Request.getInstance();


/**
 * @override
 * @param {Element} element
 */
DoctorSearch.prototype.decorateInternal = function(element) {
    DoctorSearch.base(this, 'decorateInternal', element);

    var dom = this.getView().getDom();

    this.doctorSearchControl_ = this.decorateChild(
        'doctorSearch',
        dom.doctorSearch
    );
};


/**
 * @override
 */
DoctorSearch.prototype.enterDocument = function() {
    DoctorSearch.base(this, 'enterDocument');

};

});  // goog.scope


/**
 * Instantiation
 */
(function() {
    var rootElement =
        goog.dom.getElementByClass(sh.lDoctorSearch.View.CssClass.ROOT);

    if (rootElement) {
        var view = new sh.lDoctorSearch.View(null, null, 'sber-health'),
            instance = new sh.lDoctorSearch.DoctorSearch(view);
        instance.decorate(rootElement);
    }
})();
```

#### l-doctor-search scss ####
[(на верх)](#markdown-header-table-of-content)

```scss
@import '../i-media/i-media';

.l-doctor-search {
    width: 100%;
    height: 100%;
    // padding: 0 10.5%;
}
```

#### l-doctor-search soy ####
[(на верх)](#markdown-header-table-of-content)

```soy
{namespace sh.lDoctorSearch.Template}

/**
 * DoctorSearch layout template
 * @param params {{
 *     data: {
 *          header: {},
 *          navigationMenu: {
 *              regionId = number,
 *              regionList: Array<>,
 *              regionIndex: number
 *          },
 *          doctorsData: {},
 *          search: {}
 *         }
 *     },
 *     config: {
 *      staticVersion: ?string
 *      }
 * }}
 */
{template .doctorSearch}
    {let $title kind="text"}
        {call .title_ data="all" /}
    {/let}
    {let $content kind="html"}
        {call .content_ data="all" /}
    {/let}
    {call sh.iLayout.TemplateSberHealth.base}
        {param params: [
            'data': [
                'title': $title,
                'content': $content,
                'extraScripts': [
                    'l-doctor-search.js'

                ],
                'header': [
                    'data': $params?.data?.header,
                    'config': [
                        'isBackgrounColor': true
                    ]
                ],
                'search': [
                    'data': $params?.data?.search
                ],
                'navigationMenu': $params?.data?.navigationMenu
            ],
            'config': [
                'page': 'Doctor-search',
                'hasSearchMenu': true,
                'hasNavigationMenu': true,
                'staticVersion': $params?.config?.staticVersion
            ]
        ] /}
    {/call}
{/template}


/**
 * Page title
 * @param? params {{
 *     data: {
 *         title: ?string
 *     }
 * }}
 */
{template .title_ private="true" kind="text"}
    {if $params?.data?.title}
        {$params.data.title}{sp}|{sp}
    {/if}
    Сбербанк здоровье
{/template}

/**
 * Page content
 *  @param? params {{
 *     data: {
 *          doctorsData: {},
 *          specialityList: {},
 *          search: {},
 *          specialityDescription: {?Object},
 *          filter: {}
 *     }
 * }}
 */
{template .content_ private="true"}
    <div class="l-doctor-search">
        <div class="l-doctor-search__doctor-search">
            {call cl.iFactory.FactoryManager.INSTANCE.renderTemplate}
                {param params: [
                    'factory': 'sber-health',
                    'type': 'doctorSearch',
                    'renderParams': $params?.data
                ] /}
            {/call}
        </div>
    </div>
{/template}

```

#### l-doctor-search view js ####
[(на верх)](#markdown-header-table-of-content)

```js
goog.provide('sh.lDoctorSearch.View');

goog.require('goog.object');
goog.require('sh.iLayout.ViewSberHealth');
goog.require('sh.lDoctorSearch.bDoctorSearch.View');


goog.scope(function() {



/**
 * DoctorSearch layout view
 * @param {Object=} opt_params
 * @param {string=} opt_type
 * @param {string=} opt_modifier
 * @constructor
 * @extends {sh.iLayout.ViewSberHealth}
 */
sh.lDoctorSearch.View = function(opt_params, opt_type, opt_modifier) {
    sh.lDoctorSearch.View.base(this, 'constructor',
                               opt_params, opt_type, opt_modifier);

    /**
     * DoctorSearch control element
     * @type {Element}
     */
    this.dom.doctorSearch = null;
};
goog.inherits(sh.lDoctorSearch.View, sh.iLayout.ViewSberHealth);


var View = sh.lDoctorSearch.View,
    DoctorSearchView = sh.lDoctorSearch.bDoctorSearch.View;


/**
 * List of CSS classes
 * @enum {string}
 */
View.CssClass = {
    ROOT: 'l-doctor-search'
};


/**
 * @override
 * @param {Element} element
 */
View.prototype.decorateInternal = function(element) {
    View.base(this, 'decorateInternal', element);

    this.initDataParams_(element);

    this.dom.doctorSearch =
        this.getElementByClass(DoctorSearchView.CssClass.ROOT);
};


/**
 * @param {Element} element
 * @private
 */
View.prototype.initDataParams_ = function(element) {
    try {
        if (goog.object.isEmpty(this.params)) {
            this.params = JSON.parse(element.getAttribute('data-params'));
        }
    } catch (error) {
        this.params = {};
    }
};


/**
 * @override
 */
View.prototype.enterDocument = function() {
    View.base(this, 'enterDocument');
};

});  // goog.scope

```

#### b-doctor-search js ####
[(на верх)](#markdown-header-table-of-content)

```js
goog.provide('sh.lDoctorSearch.bDoctorSearch.DoctorSearch');

goog.require('cl.iControl.Control');
goog.require('cl.iRequest.Request');
goog.require('goog.array');
goog.require('goog.dom.classlist');
goog.require('goog.events.EventType');
goog.require('goog.ui.Component.State');
goog.require('sh.bFilter.Filter');
goog.require('sh.bShPagination.ShPagination');
goog.require('sh.iUtils.Utils');
goog.require('sh.lDoctorSearch.bDoctorSearch.View');

goog.scope(function() {



/**
 * Pablo DoctorSearch block
 * @param {sh.lDoctorSearch.bDoctorSearch.View} view
 * @param {goog.dom.DomHelper=} opt_domHelper
 * @constructor
 * @extends {cl.iControl.Control}
 */
sh.lDoctorSearch.bDoctorSearch.DoctorSearch = function(view, opt_domHelper) {
    sh.lDoctorSearch.bDoctorSearch.DoctorSearch.base(this, 'constructor',
                                                     view, opt_domHelper);

    /**
     * @type {sh.bDoctorList.DoctorList}
     */
    this.doctorListControl = null;

    /**
     * @type {sh.bShPagination.ShPagination}
     */
    this.pagination = null;

    /**
     * @type {boolean}
     */
    this.isNeedRerenderPagination = false;

    /**
     * @type {sh.bFilter.Filter}
     */
    this.filter = null;

    /**
     * @type {Object}
     */
    this.filterParams = {
        name: null,
        checkboxes: null
    };

    /**
     * @type {cl.iRequest.Request}
     */
    this.request = cl.iRequest.Request.getInstance();

    this.setSupportedState(goog.ui.Component.State.ALL, false);
};
goog.inherits(sh.lDoctorSearch.bDoctorSearch.DoctorSearch, cl.iControl.Control);


var DoctorSearch = sh.lDoctorSearch.bDoctorSearch.DoctorSearch;
var Pagination = sh.bShPagination.ShPagination;
var View = sh.lDoctorSearch.bDoctorSearch.View;
var Filter = sh.bFilter.Filter;
var Utils = sh.iUtils.Utils;


var DEFAULT_FILTER = 'rating';
var FILTER_STICKY_POINT = 70;


/**
 * @override
 * @param {Element} element
 */
DoctorSearch.prototype.decorateInternal = function(element) {
    DoctorSearch.base(this, 'decorateInternal', element);

    this.params.idData = this.getView().getDataParams();

    var dom = this.getView().getDom();

    this.doctorListControl =
        this.decorateChild('bDoctorList', dom.doctorList);

    if (dom.pagination) {
        this.pagination = this.decorateChild('shPagination', dom.pagination);
    }

    if (dom.filter) {
        this.filter = this.decorateChild('filter', dom.filter);
    }

    this.onScroll();
};


/**
 * @override
 */
DoctorSearch.prototype.enterDocument = function() {
    DoctorSearch.base(this, 'enterDocument');

    var handler = this.getHandler();

    if (this.pagination) {
        handler.listen(
            this.pagination,
            Pagination.Event.CLICK,
            this.onPaginationClick
        );
    }

    if (this.filter) {
        handler.listen(
            this.filter,
            Filter.Event.REFRESH,
            this.onInitFilterChange_
        );
        handler.listen(
            this.filter,
            Filter.Event.CHANGE_CHECKBOX_STATE,
            function() {
                this.isNeedRerenderPagination = true;
            }
        );
    }

    this.viewListen(View.Event.SCROLL, this.onScroll);
};


/**
 * @param {Object} event
 */
DoctorSearch.prototype.onPaginationClick = function(event) {
    var selectPage = this.pagination.getCurrentPage();
    this.changeDoctorList_(event.searchParams);
};


/**
 * @private
 * @param {Object} event
 */
DoctorSearch.prototype.onInitFilterChange_ = function(event) {
    this.filterParams.name = event.data.filterName;
    this.filterParams.checkboxes = event.data.checkboxes;
    this.isNeedRerenderPagination = true;

    this.changeDoctorList_({
        positionFrom: 0,
        positionTo: 10
    });

    window.scrollTo(0, 0);
};


/**
 * @param {{
 *    positionFrom: number,
 *    positionTo: number
 * }} searchParams
 * @private
 */
DoctorSearch.prototype.changeDoctorList_ = function(searchParams) {
    var that = this;
    var url = this.getUrl_(searchParams.positionFrom);

    var request = this.request.send({
        'url': url,
        'type': 'get'
    });
    request.then(
        function(response) {
            that.removeDoctors_();
            that.renderDoctors_(response.data || []);
        },
        function(response) {

        },
        this
    );
};


/**
 * @param {number} positionFrom
 * @return {string}
 * @private
 */
DoctorSearch.prototype.getUrl_ = function(positionFrom) {
    var idData = this.params.idData;
    var url = '/doctorList' +
              '?positionFrom=' + positionFrom +
              '&howMuch=' + 10;

    if (idData.cityId) {
        url = url + '&cityId=' + idData.cityId;
    }

    if (idData.specialityId) {
        url = url + '&specialityId=' + idData.specialityId;
    }

    if (idData.stationId) {
        url = url + '&stationId=' + idData.stationId;
    }

    if (idData.regionId) {
        url = url + '&regionId=' + idData.regionId;
    }

    url += '&filterName=';
    url += (this.filterParams.name ? this.filterParams.name : DEFAULT_FILTER);

    if (this.filterParams.checkboxes) {
        this.filterParams.checkboxes.forEach(function(checkbox) {
            if (checkbox.value == 1) {
                url += '&' + checkbox.name + '=' + checkbox.value;
            }
        });
    }

    return url;
};


/**
 * Dispose all doctors blocks
 * @private
 */
DoctorSearch.prototype.removeDoctors_ = function() {
    this.removeChild(this.doctorListControl, true);
    this.getView().removeDoctors();
};


/**
 * Dispose all doctors blocks
 * @param {Object} doctorParams
 * @private
 */
DoctorSearch.prototype.renderDoctors_ = function(doctorParams) {
    var container = this.getView().getDom().doctorListContainer;
    var total = doctorParams.total;
    var renderParams = {
        doctorList: doctorParams.doctorList,
        filter: {
            name: this.filterParams.name,
            checkboxes: this.filterParams.checkboxes
        },
        queryParams: {
            cityId: this.params.idData.cityId,
            regionId: this.params.idData.regionId,
            stationId: this.params.idData.stationId,
            specialityId: this.params.idData.specialityId
        }
    };
    this.doctorListControl =
        this.renderChild('bDoctorList', container, renderParams);

    if (this.isNeedRerenderPagination) {
        this.pagination.reRender({
            data: {
                totalLength: total || 0,
                elementsPerPage: 10,
                pageQuantity: 5
            }
        });

        if (total) {
            var caption =
                total.toString() + ' ' + Utils.getDoctorWordEnding(total);
            this.filter.setInfoCaptionText(caption);
        }
        this.isNeedRerenderPagination = false;
    }
};


/**
 * Scroll handler
 */
DoctorSearch.prototype.onScroll = function() {
    if (this.isFilterOnTopScreen()) {
        this.setFilterSticky_(true);
    } else {
        var shift = goog.style.getClientPosition(
            this.doctorListControl.getView().getElement()
        ).y;

        if (shift > FILTER_STICKY_POINT) {
            this.setFilterSticky_(false);
        }
    }
};


/** ---------------------------------------------------------- public predicates
 * @public
 * @return {boolean}
 */
DoctorSearch.prototype.isFilterOnTopScreen = function() {
    return this.filter.getScreenPosition().y < 0;
};


/** ------------------------------------------------------------ private setters
 * @private
 * @param {boolean} sticky
 */
DoctorSearch.prototype.setFilterSticky_ = function(sticky) {
    this.filter.setSticky(sticky);
    this.getView().setInflatedFilterContainer(sticky);
};
});  // goog.scope

```

#### b-doctor-search scss ####
[(на верх)](#markdown-header-table-of-content)

```scss
@import '../../i-media/i-media';

.b-doctor-search {
    position: relative;
    box-sizing: border-box;
    width: 100%;
    height: 100%;
    margin-bottom: 100px;
    padding: 0 18px 0 35px;
}

.b-doctor-search__description {
    font-size: 18px;

    box-sizing: border-box;
    margin: 50px auto 50px;

    text-align: center;
}

.b-doctor-search__description-title {
    font-family: FedraSansProLF-Medium;
    font-size: 18px;

    display: inline-block;

    box-sizing: border-box;
    width: 100%;
    max-width: 1140px;
    margin-bottom: 14px;
    padding-left: 40px;

    text-align: left;
}

.b-doctor-search__description-content {
    font-size: 15px;
    line-height: 22px;

    display: inline-block;

    box-sizing: border-box;
    max-width: 1140px;
    padding-right: 3%;
    padding-left: 40px;

    text-align: left;
}


.b-doctor-search__filter_inflated {
    height: 78px;
}

.b-doctor-search__total-count {
    font-family: FedraSansProLF-Medium;
    font-size: 26px;
    line-height: 30px;

    box-sizing: border-box;
    margin-bottom: 36px;

    text-align: center;
}

/**
 * MEDIA M BLOCK
 */
@include media-m {

    .b-doctor-search__doctors {
        margin-top: -20px;
    }
}
/** END MEDIA M BLOCK */

/**
 * MEDIA S BLOCK
 */
@include media-s {
    .b-doctor-search {
        padding: 0;
    }
    .b-doctor-search__total-count-content {
        max-width: 100%;
        margin-top: 10px;
        padding: 0;

        text-align: center;
    }
    .b-doctor-search__description {
        display: none;
    }
}

```

#### b-doctor-search soy ####
[(на верх)](#markdown-header-table-of-content)

```soy
{namespace sh.lDoctorSearch.bDoctorSearch.Template}

/**
 * Template for Sber health DoctorSearch block
 * @constructor
 * @extends cl.iControl.Template
 */
{template .doctorSearch}
    {call .init data="all" /}
{/template}


/**
 * Root class
 * @override
 */
{template .rootClass kind="text"}
    b-doctor-search
{/template}


/**
 * Body
 *  @param? params {{
 *      doctorsData: {
 *          total: number,
 *          doctorList: [{}],
 *      },
 *      specialityDescription: {?Object}
 * }}
 */
{template .body}
    <div class="{call .rootClass /}__filter">
        {call .filter data="all" /}
    </div>
    <div class="{call .rootClass /}__doctors">
        {call cl.iFactory.FactoryManager.INSTANCE.renderTemplate}
            {param params: [
                'factory': 'sber-health',
                'type': 'bDoctorList',
                'renderParams': [
                    'doctorList': $params.doctorsData.doctorList,
                    'filter': $params.filter,
                    'queryParams': [
                        'cityId': $params.search.searchParam.idData.cityId,
                        'diagnosticName': $params.diagnosticName,
                        'specialityId': $params.search.searchParam.idData.specialityId,
                        'stationId': $params.search.searchParam.idData.stationId,
                        'regionId': $params.search.searchParam.idData.regionId
                    ]
                ]
            ] /}
        {/call}
    </div>

    {if $params.doctorsData.total != 0}
        <div class="{call .rootClass /}__pagination">
            {call cl.iFactory.FactoryManager.INSTANCE.renderTemplate}
                {param params: [
                    'factory': 'sber-health',
                    'type': 'shPagination',
                    'renderParams': [
                        'data': [
                            'totalLength': $params.doctorsData.total,
                            'elementsPerPage': 10,
                            'pageQuantity': 5
                        ]
                    ]
                ] /}
            {/call}
        </div>
    {/if}
{/template}


/**
 * Result filters
 *
 *  @param params {{
 *      doctorsData: {
 *          total: number
 *      },
 *      specialityDescription: {?Object}
 * }}
 */
{template .filter}
    {let $docLabel kind="text"}
        {call sh.iUtils.Utils.getDoctorWordEnding}
            {param count: $params.doctorsData.total /}
        {/call}
    {/let}

    {call .renderChild}
        {param params: [
            'type': 'filter',
            'renderParams': [
                'data': [
                    'info': [
                        'image': 'doctor',
                        'count': $params.doctorsData.total,
                        'title': $params?.specialityDescription?.title,
                        'description': $params?.specialityDescription?.description, 
                        'caption': $docLabel
                    ],
                    'buttons': [
                        [
                            'name': 'rating',
                            'caption': 'РџРѕ СЂРµР№С‚РёРЅРіСѓ'
                        ],[
                            'name': 'price',
                            'caption': 'РџРѕ СЃС‚РѕРёРјРѕСЃС‚Рё'
                        ],[
                            'name': 'experience',
                            'caption': 'РџРѕ СЃС‚Р°Р¶Сѓ'
                        ]
                    ],
                    'checkboxes': [
                        [
                            'name': 'atHome',
                            'caption': 'РќР° РґРѕРј',
                            'isActive': $params.filter.checkboxes[0].value == 1
                        ],[
                            'name': 'forChildren',
                            'caption': 'Р”РµС‚СЃРєРёР№ РІСЂР°С‡',
                            'isActive': $params.filter.checkboxes[1].value == 1
                        ]
                    ],
                    'filterName': $params.filter.name
                ]
            ]
        ] /}
    {/call}
{/template}


/**
 *  @param? params {{
 *     search: {
 *         searchParam: {
 *             indexData: {},
 *             idData: {
 *                 specialityId: number,
 *                 stationId: number,
 *                 regionId: number,
 *                 cityId: number
 *             }
 *         }
 *     }
 * }}
 * @override
 */
{template .attributes kind="attributes"}
    {let $idData: $params.search.searchParam.idData /}
    data-params="{call goog.json.serialize }
        {param params: [
            'specialityId': $idData.specialityId,
            'stationId': $idData.stationId,
            'regionId': $idData.regionId,
            'cityId': $idData.cityId
        ] /}
    {/call}"
{/template}


/**
 * @protected
 * @param iconType {string}
 */
{template .iconHelper}
    {call .renderChild}
        {param params: [
            'type': 'icon',
            'renderParams': [
                'data': [
                    'type': $iconType
                ],
                'config': []
            ]
        ] /}
    {/call}
{/template}

```

#### b-doctor-search view js ####
[(на верх)](#markdown-header-table-of-content)

```js
goog.provide('sh.lDoctorSearch.bDoctorSearch.View');

goog.require('cl.iControl.View');
goog.require('goog.dom');
goog.require('goog.events');
goog.require('sh.bDoctorList.View');
goog.require('sh.bFilter.View');
goog.require('sh.bShPagination.View');

goog.scope(function() {



/**
 * @constructor
 * @param {Object=} opt_params
 * @param {string=} opt_type
 * @param {String=} opt_modifier
 * @extends {cl.iControl.View}
 */
sh.lDoctorSearch.bDoctorSearch.View =
    function(opt_params, opt_type, opt_modifier) {
    View.base(
        this,
        'constructor',
        opt_params,
        opt_type,
        opt_modifier
    );

    /**
     * @protected
     * @type {Element}
     */
    this.dom.filter = null;

    this.dom.doctorList = null;
};
goog.inherits(sh.lDoctorSearch.bDoctorSearch.View, cl.iControl.View);


var View = sh.lDoctorSearch.bDoctorSearch.View;
var DoctorListView = sh.bDoctorList.View;
var Pagination = sh.bShPagination.View;
var FilterView = sh.bFilter.View;


/**
 * @const
 * @enum {string}
 */
View.CssClass = {
    ROOT: 'b-doctor-search',
    DOCTORS_LIST_CONTAINER: 'b-doctor-search__doctors',
    FILTER_INFLATED: 'b-doctor-search__filter_inflated'
};


/**
 * @const
 * @enum {string}
 */
View.Event = {
    SCROLL: 'doctor-search-scroll'
};


/** ------------------------------------------------------------------ overrides
 * @override
 * @param {Element} element
 */
View.prototype.decorateInternal = function(element) {
    View.base(this, 'decorateInternal', element);

    this.dom.doctorListContainer =
        this.getElementByClass(View.CssClass.DOCTORS_LIST_CONTAINER);

    this.dom.doctorList =
        this.getElementByClass(DoctorListView.CssClass.ROOT);

    this.dom.pagination =
        this.getElementByClass(Pagination.CssClass.ROOT);

    this.dom.filter = this.getElementByClass(FilterView.CssClass.ROOT);
};


/**
 * @override
 */
View.prototype.enterDocument = function() {
    View.base(this, 'enterDocument');

    this.getHandler().listen(
        window,
        goog.events.EventType.SCROLL,
        function() {
            this.dispatchEvent(View.Event.SCROLL);
        }
    );

};


/**
 * @override
 */
View.prototype.removeDoctors = function() {
    goog.dom.removeNode(this.dom.doctorList);
};


/** ---------------------------------------------------------- protected getters
 * @protected
 * @return {Object}
 */
View.prototype.getDataParams = function() {
    return goog.json.parse(
        this.getElement().getAttribute('data-params')
    ).params;
};


/** ---------------------------------------------------------- protected setters
 * @protected
 * @param {boolean} inflated
 */
View.prototype.setInflatedFilterContainer = function(inflated) {
    goog.dom.classlist.enable(
        goog.dom.getParentElement(this.dom.filter),
        View.CssClass.FILTER_INFLATED,
        inflated
    );
};
});  // goog.scope

```

#### b-doctor-list js ####
[(на верх)](#markdown-header-table-of-content)

```js
goog.provide('sh.bDoctorList.DoctorList');

goog.require('cl.iControl.Control');
goog.require('goog.ui.Component.State');


goog.scope(function() {



/**
 * DoctorList block
 * @param {sh.bDoctorList.View} view
 * @param {goog.dom.DomHelper=} opt_domHelper
 * @constructor
 * @extends {cl.iControl.Control}
 */
sh.bDoctorList.DoctorList = function(view, opt_domHelper) {
    sh.bDoctorList.DoctorList.base(this, 'constructor',
                                   view, opt_domHelper);

    this.setSupportedState(goog.ui.Component.State.ALL, false);
};
goog.inherits(sh.bDoctorList.DoctorList, cl.iControl.Control);

var DoctorList = sh.bDoctorList.DoctorList;


/**
 * @override
 * @param {Element} element
 */
DoctorList.prototype.decorateInternal = function(element) {
    DoctorList.base(this, 'decorateInternal', element);

    var dom = this.getView().getDom();

    for (var i = 0; i < dom.doctors.length; i++) {
        this.decorateChild('bDoctorItem', dom.doctors[i]);
    }
};

});  // goog.scope

```

#### b-doctor-list scss ####
[(на верх)](#markdown-header-table-of-content)

```scss
.b-doctor-list {
    text-align: center;
}

```

#### b-doctor-list soy ####
[(на верх)](#markdown-header-table-of-content)

```soy
{namespace sh.bDoctorList.Template}

/**
 * Template for Sber health DoctorList block
 * @constructor
 * @extends cl.iControl.Template
 */
{template .doctorList}
    {call .init data="all" /}
{/template}


/**
 * Root class
 * @override
 */
{template .rootClass kind="text"}
    b-doctor-list
{/template}


/**
 * Body
 * @param params {{
 *    doctorList: [
 *        id: number,
 *        name: {
 *            surname: string,
 *             firstname: string
 *        },
 *        rating: number,
 *        price: number,
 *        img: string,
 *        opinionCount: number,
 *        about: {
 *            description: string,
 *            isLimitOverflow: boolean
 *        },
 *        experience: number,
 *        category: string,
 *        degree: string,
 *        specialities: Array[
 *            id: number,
 *            name: string
 *        ]
 *    ],
 *    cityId: number,
 *    filter: {
 *        name: string,
 *        checkboxes: Array<{
 *            name: string,
 *            value: number
 *        }>
 *    }
 * }}
 * @override
 */
{template .body}
    {foreach $doctor in $params?.doctorList}
        {let $randomNumber kind="text"}
            {call sh.iUtils.Utils.getRandomNumber /}
        {/let}
        {call cl.iFactory.FactoryManager.INSTANCE.renderTemplate}
            {param params: [
                'factory': 'sber-health',
                'type': 'bDoctorItem',
                'renderParams': [
                    'data': $doctor,
                    'queryParams': $params.queryParams,
                    'filter': $params.filter,
                    'randomNumber': $randomNumber
                ]
            ] /}
        {/call}
    {/foreach}
{/template}

```

#### b-doctor-list view js ####
[(на верх)](#markdown-header-table-of-content)

```js
goog.provide('sh.bDoctorList.View');

goog.require('cl.iControl.View');
goog.require('sh.bDoctorItem.View');


goog.scope(function() {



/**
 * View for DoctorList block
 * @param {Object=} opt_params
 * @param {string=} opt_type
 * @param {String=} opt_modifier
 * @constructor
 * @extends {cl.iControl.View}
 */
sh.bDoctorList.View = function(opt_params, opt_type, opt_modifier) {
    sh.bDoctorList.View.base(this, 'constructor',
                             opt_params, opt_type, opt_modifier);

    /**
     * @type {{
     *    doctors: Array<Element>
     * }}
     */
    this.dom;
};
goog.inherits(sh.bDoctorList.View, cl.iControl.View);


var View = sh.bDoctorList.View;
var ItemView = sh.bDoctorItem.View;


/**
 * List of CSS classes
 * @enum {string}
 */
View.CssClass = {
    ROOT: 'b-doctor-list',
    DOCTOR_LIST_CONTAINER: 'b-doctor-list__content'
};


/**
 * @override
 * @param {Element} element
 */
View.prototype.decorateInternal = function(element) {
    View.base(this, 'decorateInternal', element);

    this.dom.doctors =
        this.getElementsByClass(ItemView.CssClass.ROOT);
};


/**
 * @override
 */
View.prototype.enterDocument = function() {
    View.base(this, 'enterDocument');
};

});  // goog.scope

```

#### b-doctor-item js ####
[(на верх)](#markdown-header-table-of-content)

```js
goog.provide('sh.bDoctorItem.DoctorItem');

goog.require('cl.gButton.Button');
goog.require('cl.iControl.Control');
goog.require('goog.ui.Component.State');
goog.require('sh.bDoctorItem.View');



/**
 * DoctorItem block
 * @param {sh.bDoctorItem.View} view
 * @param {goog.dom.DomHelper=} opt_domHelper
 * @constructor
 * @extends {cl.iControl.Control}
 */
sh.bDoctorItem.DoctorItem = function(view, opt_domHelper) {
    goog.base(this, view, opt_domHelper);

    /**
     * @type {<sh.bMetroIndicator.MetroIndicator>}
     * @protected
     */
    this.metroIndicators = null;

    this.setSupportedState(goog.ui.Component.State.ALL, false);
};
goog.inherits(sh.bDoctorItem.DoctorItem, cl.iControl.Control);

goog.scope(function() {

var DoctorItem = sh.bDoctorItem.DoctorItem;
var View = sh.bDoctorItem.View;
var Button = cl.gButton.Button;


/**
 * @override
 * @param {Element} element
 */
DoctorItem.prototype.decorateInternal = function(element) {
    DoctorItem.base(this, 'decorateInternal', element);

    if (goog.object.isEmpty(this.params)) {
        this.params = this.getView().getDataParams();
    }
    var dom = this.getView().getDom();
    if (dom.stations) {
        for (var i = 0; i < dom.stations.length; i++) {
            this.decorateChild('bMetroIcon', dom.stations[i]);
        }
    }
    if (dom.collector) {
        this.collector =
            this.decorateChild('shWidgetDataCollector', dom.collector);
    }

    this.shedule_ = [];
    this.sheduleOpenStatus_ = [];
    for (var i = 0; i < dom.shedules.length; i++) {
        this.shedule_.push(this.decorateChild('shedule', dom.shedules[i]));
        this.sheduleOpenStatus_.push(false);
    }

    this.yamaps_ = [];
    if (dom.yamaps) {
        for (var i = 0; i < dom.yamaps.length; i++) {
            this.yamaps_[i] = this.decorateChild('shMap', dom.yamaps[i]);
        }
    }

    if (dom.widgetMobile) {
        this.widgetMobile = this.decorateChild('widget', dom.widgetMobile);
    }

    this.modalWindow_ = this.decorateChild(
        'modal',
        dom.modalWindow
    );

    this.widgetMobileButton = this.decorateChild(
        'button',
        dom.widgetMobileButton
    );

    this.decorateMetroIndicators();

    this.addWidjet();
};


/**
 * decorates metro indicators
 * @protected
 */
DoctorItem.prototype.decorateMetroIndicators = function() {
    var dom = this.getView().getDom();

    if (dom.metroIndicators) {
        this.metroIndicators =
            this.decorateChildren('MetroIndicator', dom.metroIndicators);
    }
};


/**
 */
DoctorItem.prototype.addWidjet = function() {
    DdWidget({
        id: 'DoctorButton' + this.params.id + this.params.randomNumber.content,
        pid: '9347',
        container: 'DoctorButton' + this.params.id +
            this.params.randomNumber.content,
        city: this.params.cityId,
        widget: 'Button',
        action: 'LoadWidget',
        template: 'Button_common',
        type: 'Doctor',
        // clinicId: this.params.cityId,
        doctorId: this.params.id,
        modalTemplate: 'Online'
    });

    DdWidget({
        id: 'DoctorButton' + this.params.id +
            this.params.randomNumber.content + '_opt',
        pid: '9347',
        container: 'DoctorButton' + this.params.id +
            this.params.randomNumber.content + '_opt',
        widget: 'Button',
        action: 'LoadWidget',
        template: 'Button_common',
        type: 'Doctor'
    });
};


/**
 * @override
 */
DoctorItem.prototype.enterDocument = function() {
    DoctorItem.base(this, 'enterDocument');

    this.viewListen(
        View.Event.RECORD_CLICK,
        this.onRecordButtonClick
    );

    this.viewListen(
        View.Event.SWITCH_SHEDULE,
        this.onSheduleButtonClick
    );

    this.viewListen(
        View.Event.SHOW_MAP,
        this.onShowMapButtonClick
    );

    this.viewListen(
        View.Event.CLOSE_MAP,
        this.onCloseMapButtonClick
    );

    this.viewListen(
        View.Event.CLOSE_SHEDULE,
        this.onCloseShedule
    );

    this.getHandler().listen(
        this.widgetMobileButton,
        Button.Event.CLICK,
        this.onMobileRecordButtonClick
    );
};


/**
 * @param {Object} event
 */
DoctorItem.prototype.onShowMapButtonClick = function(event) {
    var index = event.data.index;
    if (this.yamaps_) {
        if (index > this.yamaps_.length) return;
        this.yamaps_[index].setMapCenterByIndex(0);
        this.getView().showMap(index);
    }
};


/**
 * @param {Object} event
 */
DoctorItem.prototype.onMobileRecordButtonClick = function(event) {
    this.modalWindow_.show();
};


/**
 * @param {Object} event
 */
DoctorItem.prototype.onCloseMapButtonClick = function(event) {
    var index = event.data.index;
    if (this.yamaps_) {
        if (index > this.yamaps_.length) return;
        this.getView().hideMap(index);
    }
};


/**
 * @param {Object} event
 */
DoctorItem.prototype.onCloseShedule = function(event) {
    var index = event.data.index;
    this.shedule_[index].hide();
    this.sheduleOpenStatus_[index] = false;
};


/**
 * @param {Object} event
 */
DoctorItem.prototype.onRecordButtonClick = function(event) {
    var name = this.params.surname + ' ' + this.params.firstname;
    var speciality = this.params.speciality.content;
    this.collector.listen(name, speciality);
};


/**
 * @param {Object} event
 */
DoctorItem.prototype.onSheduleButtonClick = function(event) {
    if (this.shedule_.length == 0) return;
    var index = event.data.index;
    if (this.sheduleOpenStatus_[index]) {
        this.shedule_[index].hide();
        this.getView().setTitleSheduleClosed(index);
        this.sheduleOpenStatus_[index] = false;
    } else {
        this.getView().hideMap(index);
        this.shedule_[index].show();
        this.getView().setTitleSheduleOpened(index);
        this.sheduleOpenStatus_[index] = true;
    }
};
});  // goog.scope
```

#### b-doctor-item scss ####
[(на верх)](#markdown-header-table-of-content)

```scss
@import '../../i-media/i-media';

.b-doctor-item_sber-health {
    position: relative;
    display: inline-block;
    box-sizing: border-box;
    width: 100%;
    max-width: 1024px;
    min-height: 240px;
    padding: 26px 0 13px;
    text-align: left;
    color: #4a4a4a;
    border-bottom: 1px solid #2b9b35;
    background-color: #fff;
    font-family: FedraSansProLF-Book, Arial, sans-serif;
    font-size: 13px;
    line-height: 1.33;
}

.b-doctor-item_sber-health:last-child {
    border: none;
}

.b-doctor-item__map-container {
    position: relative;
    width: 100%;
    height: 400px;
    top: 0;
    right: 0;
    padding-bottom: 35px;
}

.b-doctor-item__metropoliten {
    display: inline-block;
    margin-right: 20px;
    &:last-child {
        margin-right: 0;
    }
}

.b-doctor-item__close-map {
    position: absolute;
    width: 30px;
    height: 30px;
    border: 3px solid #2b9b35;
    border-radius: 100%;
    background-color: #ffffff;
    top: 18px;
    right: 18px;
    color: #282828;
    font-size: 15px;
    line-height: 31px;
    text-align: center;
    vertical-align: middle;
    cursor: pointer;
}

.b-doctor-item__content {
    position: relative;
}

.b-doctor-item__foto {
    position: absolute;
    left: 0;
    display: inline-block;
    box-sizing: border-box;
    width: 110px;
    height: 149px;
    margin-top: 8px;
    border: solid 1px rgba(43, 155, 53, 0.37);
    cursor: pointer;
}

.b-doctor-item__image {
    width: 100%;
}

.b-doctor-item__profile {
    display: inline-block;
    box-sizing: border-box;
    max-width: 508px;
    min-height: 190px;
    margin-right: 270px;
    margin-left: 138px;
    vertical-align: top;
    cursor: pointer;
}
.b-doctor-item__profile-url {
    text-decoration: none;
    color: inherit;
    &:focus {
        outline: none;
    }
}
.b-doctor-item__about {
    display: inline-block;
}

.b-doctor-item__arrow {
    width: 16px;
    height: 6px;
    cursor: pointer;
}

.b-doctor-item__name {
    padding-bottom: 21px;
    cursor: pointer;
    font-family: FedraSansProLF-Medium, Arial, sans-serif;
    font-size: 20px;
}
.b-doctor-item__surname {
    color: #2b9b35;
}

.b-doctor-item__firstame {
    max-width: 85%;
}

.b-doctor-item__experience {
    max-width: 95%;
    padding-bottom: 2px;
}

.b-doctor-item__speciality {
    max-width: 95%;
    padding-bottom: 2px;
    color: #2b9b35;
    font-family: FedraSansProLF-Medium, Arial, sans-serif;
}

.b-doctor-item__description {
    overflow: hidden;
    max-width: 95%;
    min-height: 38px;
    cursor: pointer;
    line-height: 1.5;
}

.b-doctor-item__profile_size-S {
    display: none;
}

.b-doctor-item__address-block {
    position: relative;
    margin-left: 138px;
    padding-top: 11px;
    padding-bottom: 21px;
    border-top: 1px dashed #e2e2e2;
    &:first-child {
        border-top: none;
    }
    &:last-child {
        padding-bottom: 0;
    }
}

.b-doctor-item__address {
    display: inline-block;
    max-width: 508px;
    margin-right: 288px;
    cursor: pointer;
    text-decoration: none;
}

.b-doctor-item__anchor {
    position: absolute;
    top: -150px;
    left: 0;
}

.b-doctor-item__street {
    font-family: FedraSansProLF-Medium, Arial, sans-serif;
    font-size: 13px;
    line-height: 21px;
    color: #000000;
}

.b-doctor-item__on-map {
    cursor: pointer;
    text-align: left;
    color: #2b9b35;
    font-family: FedraSansProLF-Book, Arial, sans-serif;
    font-size: 13px;
    line-height: 32px;
    display: inline-block;
}

.b-doctor-item__on-map-icon {
    display: inline-block;
    padding-right: 6px;
}

.b-doctor-item__shedule-button {
    position: absolute;
    top: 21px;
    right: 127px;
    font-size: 13px;
    line-height: 19px;
    font-family: FedraSansProLF-Light, Arial, sans-serif;
    color: #2c8f1b;
    width: 107px;
    cursor: pointer;
}

.b-doctor-item__time-icon {
    display: inline-block;
    padding-right: 3px;
}

.b-doctor-item__shedule-button-title {
    display: inline-block;
}

.b-doctor-item__station-icon {
    display: inline-block;
    box-sizing: border-box;
    width: 12px;
    height: 12px;
    margin-right: 6px;
    vertical-align: middle;
    border-radius: 100%;
}

.b-doctor-item__station-name {
    margin-right: 20px;
    font-family: FedraSansProLF-Book, Arial, sans-serif;
    font-size: 13px;
    line-height: 21px;
}

.b-doctor-item__station {
    display: inline-block;
}

.b-doctor-item__info {
    position: absolute;
    top: 5px;
    right: 7px;
    display: inline-block;
    box-sizing: border-box;
    width: 257px;
    min-width: 165px;
    padding-top: 11px;
    text-align: center;
    vertical-align: top;
    border-radius: 5px;
    background-color: #fcfcfc;
}

.b-doctor-item__info_special-price {
    border: solid 1px #f6a623;
}

.b-doctor-item__statistics {
    position: relative;
    box-sizing: border-box;
    margin: 0 auto 8px;
}

.b-doctor-item__opinion {
    display: inline-block;
    padding-right: 38px;
    font-family: FedraSansProLF-Book, Arial, sans-serif;
    font-size: 13px;
}

.b-doctor-item__rating {
    display: inline-block;
    padding-left: 38px;
    font-family: FedraSansProLF-Book, Arial, sans-serif;
    font-size: 13px;
}

.b-doctor-item__rating-count {
    color: #3fa650;
}

.b-doctor-item__opinion-label,
.b-doctor-item__rating-label {
    font-family: FedraSansProLF-Light, Arial, sans-serif;
}

.b-doctor-item__opinion-label {
    color: #2b9b35;
}

.b-doctor-item__opinion-count,
.b-doctor-item__rating-count {
    font-family: PTSans-Caption, Arial, sans-serif;
    font-size: 40px;
}

.b-doctor-item__opinion-count {
    color: #2b9b35;
}

.b-doctor-item__rating-count {
    color: #2b9b35;
}

.b-doctor-item__widget {
    padding-bottom: 14px;
}

.b-doctor-item__widget_opt {
    display: none;
}

.b-doctor-item__price {
    margin-bottom: 11px;

    font-size: 15px;
}

.b-doctor-item__price_mobile {
    display: none;
}

.b-doctor-item__price-count {
    padding-bottom: 5px;
    font-family: FedraSansProLF-Bold, Arial, sans-serif;
    font-weight: 700;
    line-height: .92;
}

.b-doctor-item__price-count_mark {
    color: #d0021b;
}

.b-doctor-item__original-price {
    font-family: FedraSansProLF-Bold;
    text-decoration: line-through;
    line-height: 1.62;
}

.b-doctor-item__original-price-count {
    font-size: 15px;
}

.b-doctor-item__original-price-text {
    font-size: 13px;
}

.b-doctor-item__special-price {
    font-family: FedraSansProLF-Bold;
    color: #2b9b35;
}

.b-doctor-item__special-price-count {
    margin-left: 14px;
    font-size: 18px;
    line-height: 1.17;
}

.b-doctor-item__special-price-text {
    margin-left: -4px;
    font-size: 13px;
    line-height: 1.62;
}

@include media-m {
    .b-doctor-item__about {
        min-height: 177px;
        margin-bottom: 24px;
    }
    .b-doctor-item__address-block {
        margin-left: 0;
    }
}

@include media-s {

    .b-doctor-item_sber-health {
        padding: 14px 0 0 0;
        border-bottom: 7px solid #f1f1f1;
    }

    .b-doctor-item__about {
        min-height: unset;
        margin-bottom: 13px;
    }

    .b-doctor-item__content {
        position: relative;
        max-width: 100%;
        margin: 0 auto;
    }

    .b-doctor-item__profile {
        display: block;
        min-height: inherit;
        margin: auto;
        max-width: 100%;
        padding-left: 33px;
        padding-right: 20px;
    }

    .b-doctor-item__description {
        display: none;
    }

    .b-doctor-item__foto {
        position: static;
        width: 91px;
        height: 124px;
        margin: 0 38px 10px 33px;
        border: none;
    }

    .b-doctor-item__degree {
        display: none;
    }

    .b-doctor-item__info {
        position: static;
        padding-top: 5px;
        display: inline-block;
        max-width: 188px;
        height: auto;
        text-align: center;
        vertical-align: top;
        border-radius: 0;
        background-color: inherit;
    }

    .b-doctor-item__info_special-price {
        border: none;
    }

    .b-doctor-item__name {
        padding-bottom: 11px;
    }

    .b-doctor-item__speciality {
        max-width: none;
    }

    .b-doctor-item__statistics {
        margin: 0;
    }

    .b-doctor-item__price {
        display: none;

        &.b-doctor-item__price_mobile {
            display: block;
            width: auto;
            font-size: 15px;
            padding-left: 33px;
        }
    }

    .b-doctor-item__opinion {
        padding-right: 21px;
    }

    .b-doctor-item__rating {
        padding-left: 21px;
    }

    .b-doctor-item__widget {
        display: none;
    }

    .b-doctor-item__widget_opt {
        display: block;
        padding-bottom: 0;
    }

    .b-doctor-item__address-block {
        margin-left: 0;
        padding-left: 33px;
    }

    .b-doctor-item__address {
        margin: 0;
    }

    .b-doctor-item__anchor {
        top: 0;
    }

    .b-doctor-item__street {
        max-width: 300px;
        display: block;
    }

    .b-doctor-item__metro {
        display: block;
    }

    .b-doctor-item__station {
        display: block;
    }

    .b-doctor-item__shedule-button {
        display: block;
        position: static;
        padding-bottom: 10px;
    }

    .b-doctor-item__on-map {
        width: 320px;
        margin: auto;
        padding-top: 0;
        padding-right: 0;
        text-align: left;
    }

    .b-doctor-item__map-container {
        height: 298px;
        padding-bottom: 0;
    }

    .b-doctor-item__widget-mobile-button {

        .b-doctor-item-mobile-button-record {
            background-color: #2b9b35;
        }
    }
}
```

#### b-doctor-item soy ####
[(на верх)](#markdown-header-table-of-content)

```soy
{namespace sh.bDoctorItem.Template}

/**
 * Template DoctorItem block
 * @constructor
 * @extends cl.iControl.Template
 */
{template .doctorItem}
    {call .init data="all" /}
{/template}


/**
 * Root class
 * @override
 */
{template .rootClass kind="text"}
    b-doctor-item
{/template}


/**
 * Body
 * @param params {{
 *    data: {
 *        id: number,
 *        name: {
 *            surname: string,
 *            firstname: string
 *        },
 *        rating: number,
 *        price: number,
 *        img: string,
 *        opinionCount: number,
 *        about: {
 *            description: string,
 *            isLimitOverflow: boolean
 *        },
 *        experience: number,
 *        category: string,
 *        degree: string,
 *        specialities: Array[
 *           id: number,
 *           name: string
 *        ],
 *        stations: [{
 *            id: number,
 *            name: string,
 *            lineColor: string,
 *            cityId: number
 *        }],
 *        clinicSlots: [{
 *            clinicId: ?number,
 *            address: ?string,
 *            stations: [{
 *                id: ?number,
 *                name: ?string,
 *                lineColor: ?string,
 *                cityId: ?number
 *            }],
 *            longitude: ?string,
 *            latitude: ?string,
 *            shedule: [{
 *                day: ?string,
 *                month: ?string,
 *                dayOfWeek: ?string,
 *                startTime: ?string,
 *                finishTime: ?string,
 *                isWorks: ?boolean
 *            }]
 *        }]
 *    },
 *    cityId: ?number,
 *    filter: {
 *        name: string,
 *        checkboxes: Array<{
 *            name: string,
 *            value: number
 *        }>
 *    }
 * }}
 * }}
 */
{template .body}
    {let $profileUrl kind="uri"}
        /doctor/{$params.data.id}
        ?cityId={$params.queryParams.cityId}

        {if $params.queryParams.specialityId}
            &specialityId={$params.queryParams.specialityId}
        {/if}

        {if $params.queryParams.regionId}
            &regionId={$params.queryParams.regionId}
        {/if}

        {if $params.queryParams.stationId}
            &stationId={$params.queryParams.stationId}
        {/if}

        {if $params.filter.name}
            &filterName={$params.filter.name}
        {/if}

        {if $params.filter.checkboxes}
            {foreach $checkbox in $params.filter.checkboxes}
                {if $checkbox.value == 1}
                    &{$checkbox.name}=1
                {/if}
            {/foreach}
        {/if}
    {/let}

    {let $price kind="text"}
        {if $params?.data?.price}
            {if $params.data.price != 0}
                {$params.data.price} Р СѓР±.
            {else}
                Р±РµСЃРїР»Р°С‚РЅРѕ
            {/if}
        {else}
            РїРѕ Р·Р°РїСЂРѕСЃСѓ
        {/if}
    {/let}

    {let $hasSpecialPrice: $params.data.specialPrice and $params.data.specialPrice > 0 /}

    {let $priceMarkClass kind="text"}
        {if $params?.data?.price}
            {if $params.data.price == 0}
                {call .rootClass /}__price-count_mark
            {/if}
        {/if}
    {/let}

    {call cl.iFactory.FactoryManager.INSTANCE.renderTemplate}
        {param params: [
            'factory': 'sber-health',
            'type': 'shWidgetDataCollector',
            'renderParams': [
                'type': 'doctor'
            ]
        ] /}
    {/call}
    <div class="{call .rootClass /}__content">
        <a class="{call .rootClass /}__profile-url" href="{$profileUrl}">
            <div class="{call .rootClass /}__profile">
                <div class="{call .rootClass /}__about">
                    <div class="{call .rootClass /}__profile_name">
                            {call .textProfile data="all" /}
                    </div>

                    <div class="{call .rootClass /}__description">
                        {$params?.data?.about?.description}
                    </div>
                </div>
            </div>

            <div class="{call .rootClass /}__foto" >
                <img class="{call .rootClass /}__image"
                     src="{$params?.data?.img}"
                     alt="doctor-image">
            </div>
        </a>

        <div class="{call .rootClass /}__info {if $hasSpecialPrice}{sp}{call .rootClass /}__info_special-price{/if}">
            <div class="{call .rootClass /}__widget">
                <div id="DoctorButton{$params.data.id}{$params.randomNumber.content}"></div>
            </div>

            <div class="{call .rootClass /}__price">
                {call .allPrices}
                    {param params: [
                        'data': $params.data,
                        'config': [
                            'originalPriceFormatted': $price,
                            'priceMarkClass': $priceMarkClass
                        ]
                    ] /}
                {/call}
            </div>

            <div class="{call .rootClass /}__statistics">
                <a href="{$profileUrl}#opinion">
                    <div class="{call .rootClass /}__opinion">
                        <span class="{call .rootClass /}__opinion-label">
                            РћС‚Р·С‹РІС‹
                        </span>
                        <div class="{call .rootClass /}__opinion-count">
                            {$params?.data?.opinionCount}
                        </div>
                    </div>
                </a>

                <div class="{call .rootClass /}__rating">
                    <span class="{call .rootClass /}__rating-label">
                        Р РµР№С‚РёРЅРі
                    </span>
                    <div class="{call .rootClass /}__rating-count">
                        {$params?.data?.rating * 2}
                    </div>
                </div>
            </div>
            <div class="{call .rootClass /}__widget_opt">
                {call .widgetMobile data="all"/}
            </div>

        </div>

        <div class="{call .rootClass /}__price
                    {sp}{call .rootClass /}__price_mobile">
            {call .allPrices}
                {param params: [
                    'data': $params.data,
                    'config': [
                        'originalPriceFormatted': $price,
                        'priceMarkClass': $priceMarkClass,
                        'isMobile': true
                    ]
                ] /}
            {/call}
        </div>
        {call .addresses}
            {param params: $params?.data?.clinicSlots /}
            {param id: $params?.data?.id /}
        {/call}
    </div>
{/template}


/**
 * @param params {{
 *    data: {
 *         price: number,
 *        .specialPrice: number
 *    },
 *    config: {
 *        originalPriceFormatted: string,
 *        priceMarkClass: string,
 *        isMobile: (boolean|undefined)
 *    }
 * }}
 */
{template .allPrices}
    {let $hasSpecialPrice: $params.data.specialPrice and $params.data.specialPrice > 0 /}

    {if $hasSpecialPrice}
        {if $params.config.isMobile}РЎС‚РѕРёРјРѕСЃС‚СЊ РїСЂРёС‘РјР° -{sp}{/if}
        <span class="{call .rootClass /}__original-price">
            <span class="{call .rootClass /}__original-price-count">
                {$params.data.price}
            </span>
            <span class="{call .rootClass /}__original-price-text">
                Р СѓР±.
            </span>
        </span>
        <span class="{call .rootClass /}__special-price">
            <span class="{call .rootClass /}__special-price-count">
                {$params.data.specialPrice}{sp}
            </span>
            <span class="{call .rootClass /}__special-price-text">
                СЂ.
            </span>
        </span>
    {else}
        РЎС‚РѕРёРјРѕСЃС‚СЊ РїСЂРёС‘РјР° -{sp}
        <span class="{call .rootClass /}__price-count
                     {$params.config.priceMarkClass}">
            {$params.config.originalPriceFormatted}
        </span>
    {/if}
{/template}

/**
 * Card block with Doctor's information
 *
 * @param params {{
 *     data: {
 *         img: string,
 *         name: {
 *             surname: string,
 *             name: string
 *         },
 *         specialities: Array<{
 *             nominativeName: string
 *         }>
 *     }
 * }}
 */
{template .widgetMobile}
<div class="{call .rootClass /}__widget-mobile-button">
    {call .renderChild}
        {param params: [
            'type': 'button',
            'renderParams': [
                'data': [
                    'content': 'Р—Р°РїРёСЃР°С‚СЊСЃСЏ'
                ],
                'config': [
                    'theme': 'widget',
                    'customClasses': ['b-doctor-item-mobile-button-record']
                ]
            ]
        ] /}
    {/call}
</div>
{let $modalContent kind="html"}
    {call .modalContent data="all" /}
{/let}
{call .renderChild}
    {param params: [
        'type': 'modal',
        'renderParams': [
            'data': [
                'content': $modalContent,
                'title': 'Р—Р°РїРёСЃСЊ РЅР° РїСЂРёРµРј Рє РІСЂР°С‡Сѓ'
            ]
        ]
    ] /}
{/call}
{/template}


/**
 * Card block with Doctor's information
 *
 * @param params {{
 *     data: {
 *         img: string,
 *         name: {
 *             surname: string,
 *             name: string
 *         },
 *         specialities: Array<{
 *             nominativeName: string
 *         }>
 *     }
 * }}
 */
{template .modalContent}
{let $name kind="html"}
    {$params?.data?.name?.surname}{sp}
    {$params?.data?.name?.firstname}
{/let}
{let $cost kind="html"}
    {if $params?.data?.price}
        {if $params?.data?.price != 0}
            {$params?.data?.price}{sp}СЂСѓР±.
        {else}
            Р‘РµСЃРїР»Р°С‚РЅРѕ
        {/if}
    {else}
        РџРѕ Р·Р°РїСЂРѕСЃСѓ
    {/if}
{/let}
{let $address kind="html"}
    address
{/let}
{if $params?.data?.clinicSlots[0]}
    <div class="{call .rootClass /}__widget-mobile">
        {call .renderChild}
            {param params: [
                'type': 'widget',
                'renderParams': [
                    'data': [
                        'title': 'Р—Р°РїРёСЃСЊ РЅР° РїСЂРёРµРј Рє РІСЂР°С‡Сѓ',
                        'info': [
                            'pic': $params?.data?.img,
                            'title': $name,
                            'subtitle': $params?.data?.clinicSlots[0].name,
                            'addresses': [
                                $params?.data?.clinicSlots[0].address
                            ],
                            'cost': $cost,
                            'phone': $params?.data?.clinicSlots[0].phone
                        ],
                        'clinicId': $params?.data?.clinicSlots[0].clinicId,
                        'doctorId': $params?.data?.id
                    ],
                    'config': [
                        'customClasses': [
                            'b-doctor-item-widget-mobile'
                        ]
                    ],
                    'type': 'doctor'
                ]
            ] /}
        {/call}
    </div>
{/if}
{/template}



/**
 * Text about doctor profile
 * @param params {{
 *    data: {
 *        id: number,
 *        name: {
 *            surname: string,
 *            firstname: string
 *        },
 *        rating: number,
 *        price: number,
 *        img: string,
 *        opinionCount: number,
 *        about: {
 *            description: string,
 *            isLimitOverflow: boolean
 *        },
 *        experience: number,
 *        category: string,
 *        degree: string,
 *        specialities: Array[
 *           id: number,
 *           name: string
 *        ],
 *        stations: [{
 *            id: number,
 *            name: string,
 *            lineColor: string,
 *            cityId: number
 *        }]
 *    },
 *    cityId: ?number
 * }}
* }}
 */
{template .textProfile}
    <div class="{call .rootClass /}__name" >
        <div class="{call .rootClass /}__surname" >
            {$params?.data?.name.surname}
        </div>
        <div class="{call .rootClass /}__firstname" >
            {$params?.data?.name.firstname}
        </div>
    </div>
    <div class="{call .rootClass /}__speciality">
        {for $i in range($params.data.specialities.length)}
            {if $i != $params.data.specialities.length - 1}
                {$params.data.specialities[$i].nominativeName},{sp}
            {else}
                {$params.data.specialities[$i].nominativeName}
            {/if}
        {/for}
    </div>
    <div class="{call .rootClass /}__experience">
        РЎС‚Р°Р¶ {$params?.data?.experience}{sp}
        {call sh.iUtils.Utils.getExperienceWordEnding}
            {param count: $params?.data?.experience /}
        {/call}
        {if $params?.data.category}
            <span class="{call .rootClass /}__degree">
                {sp}/{sp}{$params?.data?.category}
            </span>
        {/if}
    </div>
{/template}


/**
 *
 * @param params {
 *        [{
 *            address: ?string,
 *            longitude: ?string,
 *            latitude: ?string,
 *            stations: [{
 *                id: ?number,
 *                name: ?string,
 *                lineColor: ?string,
 *                cityId: ?number
 *            }],
 *            shedule: []
 *        }]
 * }
 * @param id {?number}
 */
{template .addresses}
<div class="{call .rootClass /}__address-map-container">
{foreach $clinicSlot in $params}
    <div class="{call .rootClass /}__address-block">
    <a name="{call .rootClass /}__anchor{$id}_{index($clinicSlot)}"
    {sp}class="{call .rootClass /}__anchor"></a>
        <a href="#{call .rootClass /}__anchor{$id}_{index($clinicSlot)}"
        {sp}class="{call .rootClass /}__address">
            <div class="{call .rootClass /}__street">
                {$clinicSlot.address}
            </div>
            <div class="{call .rootClass /}__metro">
                {call .stations}
                    {param stationParams: $clinicSlot.stations /}
                    {param inline: true /}
                    {param address: [
                        'coordinates': [
                            'longitude': $clinicSlot.longitude,
                            'latitude': $clinicSlot.latitude
                        ]
                    ] /}
                {/call}
            </div>
            <div class="{call .rootClass /}__on-map">
                <div class="{call .rootClass /}__on-map-icon">
                    {call .renderChild}
                        {param params: [
                            'type': 'icon',
                            'renderParams': [
                                'data': [
                                    'type': 'on_map'
                                ],
                                'config': []
                            ]
                        ] /}
                    {/call}
                </div>
                РќР° РєР°СЂС‚Рµ
            </div>
        </a>
        <div class="{call .rootClass /}__shedule-button">
            <div class="{call .rootClass /}__time-icon">
                {call cl.iFactory.FactoryManager.INSTANCE.renderTemplate}
                    {param params: [
                        'factory': 'sber-health',
                        'type': 'icon',
                        'renderParams': [
                            'data': [
                                'type': 'clock_small'
                            ]
                        ]
                    ] /}
                {/call}
            </div>
            <div class="{call .rootClass /}__shedule-button-title">
                Р’СЂРµРјСЏ СЂР°Р±РѕС‚С‹
            </div>
        </div>
        <div class="{call .rootClass /}__shedule-container">
            {if $clinicSlot.shedule}
                {call .renderChild}
                    {param params: [
                        'type': 'shedule',
                        'renderParams': [
                            'shedule': $clinicSlot.shedule,
                            'config': [
                                'customClasses': [
                                    'b-doctor-item__shedule',
                                    'i-utils__hidden'
                                ]
                            ]
                        ]
                    ] /}
                {/call}
            {/if}
        </div>
    </div>
    <div class="{call .rootClass /}__map-container i-utils__hidden">
        {call .map}
            {param params: [
                    'data': [
                        'longitude': $clinicSlot.longitude,
                        'latitude': $clinicSlot.latitude
                    ]
                 ]
            /}
        {/call}
        <div class="{call .rootClass /}__close-map">
        X
        </div>
    </div>
{/foreach}
</div>
{/template}

/**
 *  @param stationParams {
 *      [{
 *          id: number,
 *          name: string,
 *          lineColor: string,
 *      }]
 * }
 *  @param address {
 *      coordinates: {
 *          longitude: number,
 *          latitude: number
 *      }
 *  }
 */
{template .stations}
    {let $customClass kind="html"}
        {call .rootClass /}__metropoliten
    {/let}
    {foreach $station in $stationParams}
        {call .renderChild}
            {param params: [
                'type': 'MetroIndicator',
                'renderParams': [
                    'data': [
                        'stationName': $station.name,
                        'relativeAddress': $address
                    ],
                    'config': [
                        'customClasses': [$customClass],
                        'color': $station.lineColor
                    ]
                ]
            ] /}
        {/call}
    {/foreach}
{/template}


/**
 * @param params {{
 *      data:
 *      {
 *         'longitude': ?string,
 *         'latitude': ?string
 *     },
 *      config: {
 *          isMobile: ?boolean
 *     }
 * }}
 */
{template .map}
    {let $mobile kind="html"}
        {if $params?.config?.isMobile}
            -mobile
        {/if}
    {/let}
    {let $customClass kind="html"}
        b-doctor-item__map{$mobile}
    {/let}
    {call .renderChild}
        {param params: [
            'type': 'shMap',
            'renderParams': [
                'data': [$params?.data],
                'config': [
                    'enableScrollZoom': false,
                    'alignZoomControl': 'left',
                    'sizeViewportShow': 'xs',
                    'customClasses': [$customClass]
                ]
            ]
        ] /}
    {/call}
{/template}


/**
 * @override
 *  @param params {{
 *       data: {
 *           id: number
 *       },
 *       cityId: number,
 *       randomNumber: number
 *
 * }}
 */
{template .attributes kind="attributes"}
{let $speciality kind="text"}
    {for $i in range($params.data.specialities.length)}
        {if $i == $params.data.specialities.length-1}
            {$params.data.specialities[$i].nominativeName}
        {else}
            {$params.data.specialities[$i].nominativeName},{sp}
        {/if}
    {/for}
{/let}
    data-params="
        {call cl.iUtils.Utils.stringify}
            {param json: [
                'id': $params.data.id,
                'cityId': $params.queryParams.cityId,
                'surname': $params.data.name.surname,
                'firstname': $params.data.name.firstname,
                'speciality': $speciality,
                'randomNumber': $params.randomNumber,
                'stationId': $params.queryParams.stationId,
                'regionId': $params.queryParams.regionId,
                'specialityId': $params.queryParams.specialityId
            ] /}
        {/call}"
{/template}
```

#### b-doctor-item view js ####
[(на верх)](#markdown-header-table-of-content)

```js
goog.provide('sh.bDoctorItem.View');

goog.require('cl.gModal.View');
goog.require('cl.iControl.View');
goog.require('goog.events');
goog.require('sh.bMetroIcon.View');
goog.require('sh.bShWidgetDataCollector.View');



/**
 * View for DoctorItem block
 * @param {Object=} opt_params
 * @param {string=} opt_type
 * @param {String=} opt_modifier
 * @constructor
 * @extends {cl.iControl.View}
 */
sh.bDoctorItem.View = function(opt_params,
                               opt_type,
                               opt_modifier) {
    sh.bDoctorItem.View.base(
        this, 'constructor', opt_params, opt_type, opt_modifier);

    /**
     * @type {{
     *       arrow: Element,
     *       stations: Array<Element>
     * }}
     */
    this.dom;
};
goog.inherits(sh.bDoctorItem.View, cl.iControl.View);

goog.scope(function() {

var View = sh.bDoctorItem.View;
var MetroView = sh.bMetroIcon.View;
var CollectorView = sh.bShWidgetDataCollector.View;


/**
 * List of CSS classes
 * @enum {string}
 */
View.CssClass = {
    ROOT: 'b-doctor-item',
    ARROW: 'b-doctor-item__arrow',
    WIDGET: 'b-doctor-item__widget_opt',
    SHEDULE: 'b-doctor-item__shedule',
    SHEDULE_BUTTON: 'b-doctor-item__shedule-button',
    SHEDULE_BUTTON_TITLE: 'b-doctor-item__shedule-button-title',
    SHOW_MAP_AREA: 'b-doctor-item__address',
    CLOSE_MAP_AREA: 'b-doctor-item__close-map',
    YANDEX_MAP: 'b-doctor-item__map',
    MAP_CONTAINER: 'b-doctor-item__map-container',
    METRO_INDICATOR: 'b-doctor-item__metropoliten',
    WIDGET_MOBILE_BUTTON: 'b-doctor-item-mobile-button-record',
    WIDGET_MOBILE: 'b-doctor-item-widget-mobile'
};


/**
 * Event enum
 * @type {{CLICK: string}}
 */
View.Event = {
    CLICK: 'doctor-item-arrow-click',
    RECORD_CLICK: 'doctor-item-record-click',
    SWITCH_SHEDULE: 'doctor-item-switch-shedule',
    CLOSE_SHEDULE: 'doctor-item-close-shedule',
    SHOW_MAP: 'doctor-item-show-map',
    CLOSE_MAP: 'doctor-item-close-map'
};


/**
 * @override
 * @param {Element} element
 */
View.prototype.decorateInternal = function(element) {
    View.base(this, 'decorateInternal', element);
    this.dom.arrow = this.getElementByClass(View.CssClass.ARROW);

    this.dom.metroIndicators =
        this.getElementsByClass(View.CssClass.METRO_INDICATOR);

    this.dom.stations = this.getElementsByClass(MetroView.CssClass.ROOT);
    this.dom.recordButton =
        this.getElementByClass(View.CssClass.WIDGET);
    this.dom.collector =
        this.getElementByClass(CollectorView.CssClass.ROOT);
    this.dom.shedules =
        this.getElementsByClass(View.CssClass.SHEDULE);
    this.dom.sheduleButtons =
        this.getElementsByClass(View.CssClass.SHEDULE_BUTTON);
    this.dom.sheduleButtonTitles =
        this.getElementsByClass(View.CssClass.SHEDULE_BUTTON_TITLE);
    this.dom.showMapButtons =
        this.getElementsByClass(View.CssClass.SHOW_MAP_AREA);
    this.dom.closeMapButtons =
        this.getElementsByClass(View.CssClass.CLOSE_MAP_AREA);
    this.dom.yamaps = this.getElementsByClass(View.CssClass.YANDEX_MAP);
    this.dom.yamapContainers =
        this.getElementsByClass(View.CssClass.MAP_CONTAINER);

    this.dom.widgetMobile =
        this.getElementByClass(View.CssClass.WIDGET_MOBILE);

    this.dom.modalWindow = this.getElementByClass(
        cl.gModal.View.CssClass.ROOT
    );

    this.dom.widgetMobileButton =
        this.getElementByClass(View.CssClass.WIDGET_MOBILE_BUTTON);
};


/**
 * @return {Object}
 */
View.prototype.getDataParams = function() {
    var data = goog.dom.dataset.get(this.getElement(), 'params');
    this.params = JSON.parse(data);
    return this.params;
};


/**
 * @override
 */
View.prototype.enterDocument = function() {
    View.base(this, 'enterDocument');

    this.getHandler().listen(
        this.dom.recordButton,
        goog.events.EventType.CLICK,
        this.onRecordButtonClick
    );

    for (var i = 0; i < this.dom.sheduleButtons.length; i++) {
        this.getHandler().listen(
            this.dom.sheduleButtons[i],
            goog.events.EventType.CLICK,
            this.onSheduleButtonClick.bind(this, {
                index: i
            })
        );
    }

    for (var i = 0; i < this.dom.showMapButtons.length; i++) {
        this.getHandler().listen(
            this.dom.showMapButtons[i],
            goog.events.EventType.CLICK,
            this.onShowMapButtonClick.bind(this, {
                index: i
            })
        );
    }

    for (var i = 0; i < this.dom.closeMapButtons.length; i++) {
        this.getHandler().listen(
            this.dom.closeMapButtons[i],
            goog.events.EventType.CLICK,
            this.onCloseMapButtonClick.bind(this, {
                index: i
            })
        );
    }
};


/**
 * @param {Object} params
 */
View.prototype.onShowMapButtonClick = function(params) {
    this.dispatchEvent({
        type: View.Event.SHOW_MAP,
        data: params
    });
};


/**
 * @param {Object} params
 */
View.prototype.onCloseMapButtonClick = function(params) {
    this.dispatchEvent({
        type: View.Event.CLOSE_MAP,
        data: params
    });
};


/**
 * @param {Object} params
 */
View.prototype.onSheduleButtonClick = function(params) {
    this.dispatchEvent({
        type: View.Event.SWITCH_SHEDULE,
        data: params
    });
};


/**
 * @param {Object} index
 */
View.prototype.setTitleSheduleOpened = function(index) {
    this.dom.sheduleButtonTitles[index].innerHTML = 'Р—Р°РєСЂС‹С‚СЊ';
};


/**
 * @param {Object} index
 */
View.prototype.setTitleSheduleClosed = function(index) {
    this.dom.sheduleButtonTitles[index].innerHTML = 'Р’СЂРµРјСЏ СЂР°Р±РѕС‚С‹';
};


/**
 * @param {number} index
 */
View.prototype.showMap = function(index) {
    goog.dom.classlist.remove(
        this.dom.yamapContainers[index], 'i-utils__hidden'
    );

    if (this.dom.shedules[index]) {
        this.dispatchEvent({
            type: View.Event.CLOSE_SHEDULE,
            data: {index: index}
        });
        this.dom.sheduleButtonTitles[index].innerHTML = 'Р’СЂРµРјСЏ СЂР°Р±РѕС‚С‹';
    }
};


/**
 * @param {Object} index
 */
View.prototype.hideMap = function(index) {
    goog.dom.classlist.add(
        this.dom.yamapContainers[index], 'i-utils__hidden'
    );
};


/**
 * @param {Object} event
 */
View.prototype.onRecordButtonClick = function(event) {
    this.dispatchEvent(View.Event.RECORD_CLICK);
};
});  // goog.scope
```

## Castle Quiz ##
---
[(на верх)](#markdown-header-table-of-content)

[Castle Quiz](https://clevver.me/)

> Делал звук и сопутствующую инфраструктуру  
> stack: ES6 / Google Closure Tools (Compiler/Library/Soy template) / Cordova  
> Дата релиза задачи на прод неизвестна

### Castle Quiz Code###

### i-sound js ###
[(на верх)](#markdown-header-table-of-content)

```js
goog.provide('qz.lQuiz.iSound.Sound');

goog.require('goog.Promise');
goog.require('qz.lQuiz.iCordova.Cordova');
goog.require('qz.lQuiz.iDebug.Debug');
goog.require('qz.lQuiz.iSound.CordovaMedia');
goog.require('qz.lQuiz.iSound.CordovaMediaSprite');
goog.require('qz.lQuiz.iSound.WebMedia');


goog.scope(function() {
    const
        cordovaWrap = qz.lQuiz.iCordova.Cordova.getInstance(),
        logger = qz.lQuiz.iDebug.Debug.getInstance(),
        media = cordovaWrap.isEnabled() ?
            qz.lQuiz.iSound.CordovaMedia.getInstance() :
            qz.lQuiz.iSound.WebMedia.getInstance();

    /**
     * Sound
     */
    class Sound {
        /**
         * @constructor
         */
        constructor() {
            /**
             * @private
             * @type {Object}
             */
            this.is_ = {
                init: null,
                enabled: null,
                playing: null,
                muted: null
            };
            /**
             * @private
             * @type {number}
             */
            this.volume_ = null;
        }

        /**
         * Initialize
         *
         * @return {goog.Promise}
         */
        init() {
            let props = {
                root: 'static/',
                media: 'sound/',
                name: 'sprite',
                format: 'm4a'
            };

            this.is_.init = false;
            this.is_.enabled = true;
            this.is_.playing = false;
            this.is_.muted = false;
            this.volume_ = 1;

            return new goog.Promise((resolve) => {
                try {
                    media
                        .init(props)
                        .then(() => {
                            this.is_.init = true;
                            resolve();
                        });
                    setTimeout(resolve(), 10000);
                } catch (error) {
                    this.is_.init = false;
                    logger.error(`${error}`);
                }
            });
        }

        /**
         * Gets init state
         */
        isInit() {
            return this.is_.init;
        }

        /**
         * Gets enable state
         */
        isEnabled() {
            return this.is_.enabled;
        }

        /**
         * Gets playing state
         */
        isPlaying() {
            return this.is_.playing;
        }

        /**
         * Gets mute state
         */
        isMuted() {
            return this.is_.muted;
        }

        /**
         * Plays sound
         *
         * @param {string} soundName
         * @return {goog.Promise}
         */
        play(soundName) {
            if (this.is_.init && this.is_.enabled) {
                this.is_.playing = true;

                return media
                    .play(soundName)
                    .then((stopReason) => {
                        this.onStop(stopReason);
                    });
            }
        }

        /**
         * Stops playing sound
         *
         * @return {goog.Promise}
         */
        stop() {
            if (this.is_.init) {
                if (this.is_.playing) {
                    return media.stop();
                } else {
                    return new goog.Promise.resolve();
                }
            }
        }

        /**
         * Gets volume of the sound
         */
        getVolume() {
            return this.volume_;
        }

        /**
         * Sets volume of the sound
         *
         * @param {number} value
         */
        setVolume(value) {
            if (value >= 0 && value <= 1) {
                this.volume_ = value;
                media.setVolume(value);
            } else {
                logger.error(`Sound's volume must be between 0.0 and 1.0`);
            }
        }

        /**
         * Toggle mute
         *
         * @param {boolean} isMuted
         */
        mute(isMuted = true) {
            this.is_.muted = isMuted;

            isMuted ?
                media.setVolume(0) :
                media.setVolume(this.volume_);
        }

        /**
         * Toggle enabled state
         *
         * @param {boolean} isEnabled
         */
        enable(isEnabled = true) {
            this.is_.enabled = isEnabled;
        }

        /**
         * Sets disabled
         */
        disable() {
            this.enable(false);
        }

        /**
         * On stop event handler
         *
         * @param {string} reason
         * @return {goog.Promise}
         */
        onStop(reason) {
            this.is_.playing = false;
        }
    }

    /**
     * @enum {string}
     */
    Sound.Audio = {
        BEEP: 'beep',
        BLEEP: 'bleep',
        BUTTON_WRONG: 'button-wrong',
        DOUBLE_TAP: 'double-tap',
        FANFARE: 'fanfare',
        HALLELUJAH: 'hallelujah',
        LOW_BATTERY: 'low-battery',
        MOUSE_CLICK: 'mouse-click',
        TICK_TACK: 'tick-tack'
    };

    qz.lQuiz.iSound.Sound = Sound;
    goog.addSingletonGetter(qz.lQuiz.iSound.Sound);
});  // goog.scope

```

### i-web-media js ###
[(на верх)](#markdown-header-table-of-content)

```js
goog.provide('qz.lQuiz.iSound.WebMedia');

goog.require('goog.Promise');
goog.require('qz.lQuiz.iJSONLoader.JSONLoader');


goog.scope(function() {
    const
        JSONLoader = qz.lQuiz.iJSONLoader.JSONLoader.getInstance();

    /**
     * Web Media
     */
    class WebMedia {
        /**
         * @constructor
         */
        constructor() {
            /**
             * Media instance (Howler.js extended library)
             *
             * @private
             * @type {Howl}
             */
            this.media_ = null;
            /**
             * @private
             * @type {Object}
             */
            this.props_ = null;
        }

        /**
         * Initialize
         *
         * @param {Object} props        - sprite properties
         *        {string} props.root   - root directory
         *        {string} props.media  - media directory
         *        {string} props.name   - sprite name
         *        {Array}  props.format - sprite media format
         * @return {goog.Promise}
         */
        init(props) {
            this.props_ = props;
            let sprite = `${props.media}${props.name}`;

            return new goog.Promise((resolve) => {
                JSONLoader
                    .getJSON(sprite)
                    .then((splitting) => {
                        this.props_.splitting = splitting;
                        this.media_ = new Howl({
                            src: [`${props.root}${sprite}.${props.format}`],
                            sprite: splitting.sprite
                        });
                        resolve();
                    });
                setTimeout(resolve(), 10000);
            });
        }

        /**
         * Gets properties
         *
         * @return {Object}
         */
        getProperties() {
            return this.props_;
        }

        /**
         * Plays sprite's chunk
         *
         * @param {string} chunkName
         * @return {goog.Promise}
         */
        play(chunkName) {
            return new goog.Promise((resolve) => {
                this.media_.play(chunkName);
                this.media_.once('end', () => {
                    resolve('end');
                });
                this.media_.once('stop', () => {
                    resolve('stop');
                });
            });
        }

        /**
         * Stops playing sprite's chunk
         *
         * @return {goog.Promise}
         */
        stop() {
            return new goog.Promise((resolve) => {
                this.media_.stop();
                resolve();
            });
        }

        /**
         * Sets volume of the sound
         *
         * @param {value} value
         */
        setVolume(value) {
            this.media_.volume(value);
        }
    }

    qz.lQuiz.iSound.WebMedia = WebMedia;
    goog.addSingletonGetter(qz.lQuiz.iSound.WebMedia);
});  // goog.scope

```

### i-cordova-media js ###
[(на верх)](#markdown-header-table-of-content)

```js
goog.provide('qz.lQuiz.iSound.CordovaMedia');

goog.require('goog.Promise');
goog.require('goog.string');
goog.require('qz.lQuiz.iDebug.Debug');


goog.scope(function() {
    const
        logger = qz.lQuiz.iDebug.Debug.getInstance();

    /**
     * Cordova Media
     */
    class CordovaMedia {
        /**
         * @constructor
         */
        constructor() {
            /**
             * Media instance (Media - cordova-media-plugin)
             *
             * @private
             * @type {Media}
             */
            this.sound_ = null;
            /**
             * @private
             * @type {Object}
             */
            this.props_ = null;
            /**
             * @private
             * @type {Object}
             */
            this.volume_ = null;
            /**
             * @private
             * @type {goog.Promise}
             */
            this.playPromiseResolver_ = null;
            /**
             * @private
             * type {Object}
             */
            this.soundCache_ = null;
        }

        /**
         * Initialize
         *
         * @param {Object} props        - sprite properties
         *        {string} props.root   - root directory
         *        {string} props.media  - media directory
         * @return {goog.Promise}
         */
        init(props) {
            this.props_ = props;
            this.props_.format = 'mp3';
            this.props_.platform = device.platform.toLowerCase();
            this.soundCache_ = {};
            this.volume_ = 1;

            return new goog.Promise(resolve => resolve());
        }

        /**
         * Plays mp3
         *
         * @param {string} name
         * @return {goog.Promise}
         */
        play(name) {
            return new goog.Promise((resolve) => {
                this.playPromiseResolver_ = resolve;
                let url = this.checkURL_(goog.string.buildString(
                    this.props_.root,
                    this.props_.media,
                    name,
                    '.',
                    this.props_.format
                ));

                try {
                    if (this.soundCache_[name]) {
                        this.sound_ = this.soundCache_[name];
                    } else {
                        this.sound_ = new Media(
                            url,
                            // on success
                            () => {
                                logger.log('sound success');
                                this.playPromiseResolver_();
                            },
                            () => logger.error(`sound error: ${error}`),
                            this.mediaStatus_
                        );
                        this.soundCache_[name] = this.sound_;
                    }
                    logger.log(`sound start play: ${name}`);
                    this.sound_.setVolume(this.volume_);
                    this.sound_.play();
                } catch (error) {
                    logger.error(error);
                }
            });
        }

        /**
         * Stops playing sprite's chunk
         *
         * @return {goog.Promise}
         */
        stop() {
            return new goog.Promise((resolve) => {
                this.sound_.stop();
                this.sound_.release();
                this.playPromiseResolver_();
                resolve();
            });
        }

        /**
         * Sets volume
         *
         * @param {value} value
         */
        setVolume(value) {
            this.volume_ = value;
        }

        /**
         * Checks platform's source URL
         *
         * @private
         * @param {string} nativeURL
         * @return {string}
         */
        checkURL_(nativeURL) {
            try {
                if (this.props_.platform === 'android') {
                    return "/android_asset/www/" + nativeURL;
                } else {
                    return nativeURL;
                }
            } catch (error) {
                logger.error(error);
            }
        }

        /**
         * Handler media statuses
         *
         * @private
         * @param {string} status
         */
        mediaStatus_(status) {
            if (status === Media.MEDIA_STOPPED) {
                logger.log(`sound stopped`);
            }
        }
    }

    qz.lQuiz.iSound.CordovaMedia = CordovaMedia;
    goog.addSingletonGetter(qz.lQuiz.iSound.CordovaMedia);
});  // goog.scope

```

### i-cordova-media-sprite js###
[(на верх)](#markdown-header-table-of-content)

```js
goog.provide('qz.lQuiz.iSound.CordovaMediaSprite');

goog.require('goog.Promise');
goog.require('qz.lQuiz.iDebug.Debug');
goog.require('qz.lQuiz.iJSONLoader.JSONLoader');


goog.scope(function() {
    const
        logger = qz.lQuiz.iDebug.Debug.getInstance(),
        JSONLoader = qz.lQuiz.iJSONLoader.JSONLoader.getInstance();

    /**
     * Cordova Media Sprite
     */
    class CordovaMediaSprite {
        /**
         * @constructor
         */
        constructor() {
            /**
             * Media instance (Media - cordova-media-plugin)
             *
             * @private
             * @type {Media}
             */
            this.media_ = null;
            /**
             * @private
             * @type {Object}
             */
            this.props_ = null;
            /**
             * @private
             * @type {goog.Promise}
             */
            this.playPromiseResolver_ = null;
            /**
             * @private
             * @type {Function}
             */
            this.playTimeout_ = null;
        }

        /**
         * Initialize
         *
         * @param {Object} props        - sprite properties
         *        {string} props.root   - root directory
         *        {string} props.media  - media directory
         *        {string} props.name   - sprite name
         *        {Array}  props.format - sprite media format
         * @return {goog.Promise}
         */
        init(props) {
            this.props_ = props;
            this.props_.platform = device.platform.toLowerCase();
            let sprite = `${props.media}${props.name}`;
            this.props_.url =
                this.checkURL_(`${props.root}${sprite}.${props.format}`);

            return new goog.Promise((resolve) => {
                try {
                    JSONLoader
                        .getJSON(sprite)
                        .then((splitting) => {
                            this.props_.splitting = splitting;
                            this.media_ = new Media(
                                this.props_.url,
                                () => logger.log('media success'),
                                () => logger.error(`media error: ${error}`),
                                this.mediaStatus_
                            );
                            // it's need to fix magic behaviour.
                            // In the first time 'media.seekTo(position);' not
                            // working correctly, I don't now why.
                            // If set the position the first time playing
                            // always the first sound but next time playing
                            // correctly
                            this.media_.play();
                            this.media_.stop();
                            resolve();
                        });
                    setTimeout(resolve(), 10000);
                } catch (error) {
                    logger.error(error);
                }
            });
        }

        /**
         * Plays sprite's chunk
         *
         * @param {string} chunkName
         * @return {goog.Promise}
         */
        play(chunkName) {
            return new goog.Promise((resolve) => {
                this.playPromiseResolver_ = resolve;
                let chunk = this.getChunkProps_(chunkName);
                logger.log(`media start play: ${JSON.stringify(chunk)}`);
                try {
                    this.seekTo(chunk.timeStart).then((startTime) => {
                        logger.log(`media set start time: ${startTime}`);
                        this.media_.play()
                    });
                    clearTimeout(this.playTimeout_);
                    this.playTimeout_ = setTimeout(() => {
                        this.media_.stop();
                        if (this.playPromiseResolver_) {
                            this.playPromiseResolver_();
                        }
                    }, chunk.duration);
                } catch (error) {
                    logger.error(error);
                }
            });
        }

        /**
         * Stops playing sprite's chunk
         *
         * @return {goog.Promise}
         */
        stop() {
            return new goog.Promise((resolve) => {
                if (this.status_ !== Media.MEDIA_STOPPED) {
                    this.media_.stop();
                }
                resolve();
            });
        }

        /**
         * Set audio position
         *
         * @param {number} timeStart in milliseconds
         */
        seekTo(timeStart) {
            return new goog.Promise((resolve) => {
                try {
                    this.media_.seekTo(timeStart);
                    resolve(timeStart);
                } catch (error) {
                    logger.error(error);
                }
            });
        }

        /**
         * Sets volume
         *
         * @param {value} value
         */
        setVolume(value) {
            this.media_.setVolume(value);
        }

        /**
         * Checks platform's source URL
         *
         * @private
         * @param {string} nativeURL
         * @return {string}
         */
        checkURL_(nativeURL) {
            try {
                if (this.props_.platform === 'android') {
                    return "/android_asset/www/" + nativeURL;
                } else {
                    return nativeURL;
                }
            } catch (error) {
                logger.error(error);
            }
        }

        /**
         * Parse and return chunk properties
         *
         * @private
         * @param {string} name
         *
         */
        getChunkProps_(name) {
            return {
                name: name,
                timeStart: this.props_.splitting.sprite[name][0],
                duration: this.props_.splitting.sprite[name][1]
            };
        }

        /**
         * Handler media statuses
         *
         * @private
         * @param {string} status
         */
        mediaStatus_(status) {
            if (status === Media.MEDIA_STOPPED) {
                logger.log(`media stopped`);
            }
        }
    }

    qz.lQuiz.iSound.CordovaMediaSprite = CordovaMediaSprite;
    goog.addSingletonGetter(qz.lQuiz.iSound.CordovaMediaSprite);
});  // goog.scope

```